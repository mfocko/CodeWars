use std::collections::HashMap;
use std::collections::HashSet;

type Factors = HashMap<i32, i32>;

fn as_primes(mut n: i32) -> Factors {
    let mut powers: Factors = HashMap::new();

    (2..((n as f64).sqrt() as i32) + 1).for_each(|x| {
        let mut q = 0;
        while n % x == 0 {
            n /= x;
            q += 1;
        }

        if q != 0 {
            powers.insert(x, q);
        }
    });

    if n > 1 {
        powers.insert(n, 1);
    }

    powers
}

fn get_powers(m: i32, primes: HashSet<&i32>) -> Factors {
    let mut powers: Factors = HashMap::new();

    (2..m + 1).for_each(|mut n| {
        primes.iter().for_each(|&&x| {
            let mut q = 0;
            while n % x == 0 {
                n /= x;
                q += 1;
            }

            powers.insert(x, q + powers.get(&x).unwrap_or(&0));
        });
    });

    powers
}

fn get_zeroes(base: Factors, powers: Factors) -> i32 {
    let mut adjusted: Factors = HashMap::new();

    for (prime, power) in powers {
        adjusted.insert(prime, power / base[&prime]);
    }

    *adjusted.values().min().unwrap()
}

fn zeroes(base: i32, number: i32) -> i32 {
    let base_as_primes = as_primes(base);
    let powers: Factors = get_powers(number, base_as_primes.keys().collect());
    get_zeroes(base_as_primes, powers)
}
