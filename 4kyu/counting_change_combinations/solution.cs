using System;

public static class Kata {
     public static int CountCombinations(int money, int[] coins, bool recursivelyCalled = false) {
         if (money == 0 && recursivelyCalled) return 1;
         else if (coins.GetLength(0) == 1) {
             if (money % coins[0] == 0) return 1;
             else return 0;
         } else if (!recursivelyCalled) {
             Array.Sort(coins);
             Array.Reverse(coins);
         }

         var result = 0;

         var times = money / coins[0];
         var newCoins = new int[coins.GetLength(0) - 1];

         for (var i = 0; i < coins.GetLength(0) - 1; i++)
             newCoins[i] = coins[i + 1];

         for (var i = 0; i <= times; i++) {
             result += CountCombinations(money - i * coins[0], newCoins, true);
         }

         return result;
     }
}
