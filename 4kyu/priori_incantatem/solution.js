class Wand {
  constructor(spells) {
    this.history = [];
    Object.assign(this, spells);

    return new Proxy(this, {
      get: (target, property) => {
        const val = target[property];
        if (typeof val === 'function') {
          target.history.unshift(property);
        }
        return val;
      }
    });
  }

  prioriIncantatem() {
    return this.history.slice(1, MAX_PRIOR_SPELLS + 1);
  }

  deletrius() {
    this.history = ['deletrius'];
  }
}
