function checkExtension(name) {
    const extensions = ['.html', '.htm', '.php', '.asp'];
    return name.replace(/\.html/g, '').replace(/\.htm/g, '').replace(/\.php/g, '').replace(/\.asp/g, '');
}

function shorten(name) {
    if (name.length <= 30) {
        name = name.replace(/-/g, ' ');
    } else {
        const ignore = ["the","of","in","from","by","with","and", "or", "for", "to", "at", "a"];

        name = name.split('-').filter(e => !ignore.includes(e)).map(e => e[0]);
        name = name.join('');
    }

    return name.toUpperCase();
}

function buildSegment(url, name, last=false) {
    if (last) {
        return `<span class="active">${shorten(checkExtension(name))}</span>`;
    } else {
        return `<a href="${url}">${shorten(checkExtension(name))}</a>`;
    }
}

function generateBC(url, separator) {
    console.log(url);
    if (url.includes('//')) {
      url = url.split('//')[1];
    }

    url = url.split("/").filter(e => {
        return !e.startsWith('index');
    }).map(e => {
        if (e.includes("#"))
            return e.substring(0, e.indexOf("#"));
        else if (e.includes("?"))
            return e.substring(0, e.indexOf("?"));
        else
            return e;
    });

    let result = [];
    let path = '/';
    if ((url.length == 2 && url[1] == '') || url.length == 1) {
        result.push(buildSegment('/', 'home', true));
        return result.join('');
    } else
        result.push(buildSegment('/', 'home'));


    for (let i = 1; i < url.length - 1; i++) {
        path += `${url[i]}/`;
        result.push(buildSegment(path, url[i]));
    }
    path += `/${url[url.length - 1]}`
    result.push(buildSegment(path, url[url.length - 1], true));

    return result.join(separator);
}
