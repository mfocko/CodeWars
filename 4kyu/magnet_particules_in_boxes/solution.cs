using System;

public class Magnets
{
    public static double Doubles(int maxk, int maxn)
    {
        double val = 0;

        for (int k = 1; k <= maxk; k++)
            for (int n = 1; n <= maxn; n++)
                val += 1 / (k * Math.Pow(n + 1, 2 * k));

        return val;
    }
}
