import java.util.PriorityQueue;

public class Hamming {
    private static void update(PriorityQueue<Long> q, long n) {
        q.add(2*n);
        q.add(3*n);
        q.add(5*n);
    }

    public static long hamming(int n) {
        PriorityQueue<Long> cache = new PriorityQueue<>();
        update(cache, 1);

        long smallest = 1;
        for (int i = 1; i < n; i++) {
            smallest = cache.poll();
            while (cache.peek() == smallest) {
                cache.poll();
            }

            update(cache, smallest);
        }

        return smallest;
    }
}
