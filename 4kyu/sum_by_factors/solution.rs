use std::vec::Vec;

fn is_prime(n: i64) -> bool {
    match n {
        x if x % 2 == 0 => x == 2,
        _ => {
            let max_value = (n as f64).sqrt().ceil() as i64;
            (3..max_value + 1).step_by(2).all(|i| n % i != 0)
        }
    }
}

pub fn sum_of_divided(l: Vec<i64>) -> Vec<(i64, i64)> {
    let max_value = l.iter().max_by_key(|x| x.abs());

    match max_value {
        Some(max_val) => (2..max_val.abs() + 1)
            .filter(|&i| is_prime(i) && l.iter().any(|x| x % i == 0))
            .map(|i| {
                (
                    i,
                    l.iter()
                        .fold(0, |acc, &x| acc + if x % i == 0 { x } else { 0 }),
                )
            })
            .collect(),
        None => vec![],
    }
}
