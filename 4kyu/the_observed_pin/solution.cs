using System.Collections.Generic;
using System.Linq;

public class Kata {
  private static Dictionary<char, string> Possible = new Dictionary<char, string>() {
    { '1', "124"}, { '2', "1235" }, { '3', "236" },
    { '4', "1457"}, { '5', "24568" }, { '6', "3569" },
    { '7', "478"}, { '8', "57890" }, { '9', "689" },
    { '0', "80" }
  };

  public static List<string> GetPINs(string observed) {
    var result = new HashSet<string>() {
      ""
    };

    foreach (char observedDigit in observed) {
      var possibleDigits = Possible[observedDigit];

      result = new HashSet<string>(
        result
        .Select(pin => possibleDigits.Select(d => pin + d))
        .Aggregate((acc, e) => acc.Union(e))
      );
    }

    return result.ToList();
  }
}
