using System;

public class Kata {
    public static (uint remainder, uint digit) SumTwo(char a, char b, uint c = 0) {
        uint val_a = (uint) a - 48;
        uint val_b = (uint) b - 48;

        var sum = val_a + val_b + c;
        return (sum / 10, sum % 10);
    }
    public static string Add(string a, string b) {
        var result = "";

        if (a.Length > b.Length) {
            var temp = a;
            a = b;
            b = temp;
        }

        int idx_a = a.Length - 1;
        int idx_b = b.Length - 1;
        uint remainder = 0;
        uint last_digit = 0;

        while (idx_a >= 0) {
            (remainder, last_digit) = SumTwo(a[idx_a], b[idx_b], remainder);
            result = last_digit.ToString() + result;
            idx_a--;
            idx_b--;
        }

        while (idx_b >= 0) {
            (remainder, last_digit) = SumTwo('0', b[idx_b], remainder);
            result = last_digit.ToString() + result;
            idx_b--;
        }

        if (remainder > 0) {
            result = remainder.ToString() + result;
        }

        return result;
    }
}
