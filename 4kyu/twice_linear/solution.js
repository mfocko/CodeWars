function dblLinear(n) {
    let values = [1];
    let seen = new Set(values);
    let index = 0;
    let length = 0;

    while (length < n) {
        const x = values.shift();
        seen.delete(x);

        const a = 2 * x + 1;
        const b = 3 * x + 1;

        if (!seen.has(a)) {
            let i = index;

            for (;i < values.length; i++)
              if (values[i] > a)
                break;
            values.splice(i, 0, a);
            seen.add(a);
            index = i;
        }

        seen.add(b);
        values.push(b);
        length++;
    }
    console.log(values);
    return values[0];
}
