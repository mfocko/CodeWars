using System;

public static class Kata {
    public static string sumStrings(string a, string b) {
        var result = "";
        var i = a.Length - 1;
        var j = b.Length - 1;
        var remainder = 0;

        while (i >= 0 && j >= 0) {
            var a_digit = a[i] - '0';
            var b_digit = b[j] - '0';
            var sum = a_digit + b_digit + remainder;

            result = (sum % 10) + result;

            if (sum >= 10) {
                remainder = sum / 10;
            } else {
                remainder = 0;
            }

            i--;
            j--;
        }

        while (i >= 0) {
            var a_digit = a[i] - '0';
            var sum = a_digit + remainder;

            result = (sum % 10) + result;

            if (sum >= 10) {
                remainder = sum / 10;
            } else {
                remainder = 0;
            }

            i--;
        }

        while (j >= 0) {
            var b_digit = b[j] - '0';
            var sum = b_digit + remainder;

            result = (sum % 10) + result;

            if (sum >= 10) {
                remainder = sum / 10;
            } else {
                remainder = 0;
            }

            j--;
        }

        if (remainder > 0) {
            result = remainder + result;
        }

        return result.TrimStart('0');
    }
}
