function treeByLevels (rootNode) {
	let queue = [rootNode];
  let values = [];

  while (queue.length > 0) {
    let node = queue.shift();
    if (node === null) {
      continue;
    }

    values.push(node.value);

    queue.push(node.left);
    queue.push(node.right);
  }

  return values;
}
