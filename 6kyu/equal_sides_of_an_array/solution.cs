public class Kata
{
  public static int SumOfTheArray(int[] arr)
  {
    int sum = 0;
    for (int i = 0, max = arr.Length; i < max; sum += arr[i++]);
    return sum;
  }
  public static int FindEvenIndex(int[] arr)
  {
    int left, right;
    left = 0;
    right = SumOfTheArray(arr) - arr[0];
    for (int i = 1, max = arr.Length; i < max; i++)
    {
      left += arr[i - 1];
      right -= arr[i];
      if (left == right) return i;
    }

    return -1;
  }
}
