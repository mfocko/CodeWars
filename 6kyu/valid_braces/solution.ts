export function validBraces(braces: string): boolean {
  let stack: string[] = [];

  for (let b of braces) {
    switch (b) {
        case "(":
          stack.push(")");
          break;
        case "[":
          stack.push("]");
          break;
        case "{":
          stack.push("}");
          break;
        default:
          if (stack.length == 0 || stack.pop() != b) {
            return false;
          }
    }
  }

  return stack.length == 0;
}
