using System.Collections.Generic;
using System.Linq;
using System;

public class Kata
{
  public static string SpinWords(string sentence)
  {
    string[] words = sentence.Split(' ');
    string result = "";

    foreach (string word in words)
    {
      if (word.Length >= 5)
      {
        result += new string(word.ToCharArray().Reverse().ToArray()) + " ";
      }
      else result += word + " ";
    }
    return result.Trim();
  }
}
