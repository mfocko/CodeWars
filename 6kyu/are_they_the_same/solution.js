function comp(array1, array2){
  let squares = [];
  for (let num of array1) {
    squares.push(num * num);
  }

  if (!array2)
    return false;

  for (let square of array2) {
    let index = squares.indexOf(square);
    if (index != -1) squares.splice(index, 1);
  }
  if (squares.length > 0) return false;
  else return true;
}
