package multiplicationtable

func MultiplicationTable(size int) [][]int {
  result := make([][]int, size);
  for i := 0; i < size; i++ {
    result[i] = make([]int, size);
  }

  for i := 0; i < size; i++ {
    for j := 0; j < size; j++ {
      result[i][j] = (i + 1) * (j + 1);
    }
  }

  return result;
}
