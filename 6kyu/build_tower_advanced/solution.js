String.prototype.centerJustify = function(length, char) {
  let i = 0;
  let str = this;
  let toggle = true;
  while (i + this.length < length) {
    i++;
    if (toggle)
      str = str + char;
    else
      str = char + str;
    toggle = !toggle;
  }
  return str;
}

function towerBuilder(nFloors, nBlockSz) {
  let floor = '';
  for (let i = 0; i < nBlockSz[0]; i++)
    floor += '*';
  const width = nFloors * 2 * nBlockSz[0] - nBlockSz[0];
  let result = [];
  for (let i = 0, count = 1; i < nFloors; i++, count += 2) {
    for (let j = 0; j < nBlockSz[1]; j++) {
      result.push(floor.centerJustify(width, ' '));
    }
    for (let j = 0; j < nBlockSz[0]; j++)
      floor += '**';
  }
  return result;
}
