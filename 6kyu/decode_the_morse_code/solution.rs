mod preloaded;
use preloaded::MORSE_CODE;
// MORSE_CODE is `HashMap<String, String>`. e.g. ".-" -> "A".

fn decode_morse(encoded: &str) -> String {
    let mut result = String::new();

    encoded.split("   ").map(|x| x.trim()).for_each(|x| {
        x.split_whitespace().for_each(|c| {
            result += MORSE_CODE.get(c).unwrap();
        });

        result += " ";
    });


    String::from(result.trim())
}
