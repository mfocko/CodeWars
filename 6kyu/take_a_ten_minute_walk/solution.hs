module Codewars.Kata.TenMinuteWalk where

isValidWalk :: [Char] -> Bool
isValidWalk walk = (length . take 11) walk == 10 && (0, 0) == foldl change (0, 0) walk
  where change (x, y) d = case d of
          'n' -> (x, y + 1)
          's' -> (x, y - 1)
          'e' -> (x + 1, y)
          'w' -> (x - 1, y)
          _ -> error "invalid move"
