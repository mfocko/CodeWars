using System;

public class Kata
{
    public static int[] FilterOutZeroes(int[] queue) => Array.FindAll(queue, e => e > 0);

    public static long QueueTime(int[] customers, int n)
    {
        var sum = 0l;

        while (customers.Length > 0)
        {
            var upTo = Math.Min(n, customers.Length);
            var shortest = customers[0];
            for (var i = 1; i < upTo; i++)
                if (customers[i] < shortest) shortest = customers[i];

            sum += shortest;
            for (var i = 0; i < upTo; i++) customers[i] -= shortest;
            customers = FilterOutZeroes(customers);
        }

        return sum;
    }
}
