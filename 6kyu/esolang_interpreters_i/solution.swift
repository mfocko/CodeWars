func interpreter(_ prog: String) -> String {
  var cell = 0
  var result = ""

  for i in prog {
    switch i {
      case "+":
        cell = (cell + 1) % 256
      case ".":
        result += String(Character(Unicode.Scalar(cell)!))
      default:
        break
    }
  }

  return result
}
