using System;

namespace Solution
{
  class Kata
  {
    public static int find_it(int[] seq)
    {
      Array.Sort(seq);

      int count = 1;

      for (int i = 1; i < seq.Length; i++)
      {
        if (seq[i] == seq[i - 1]) count++;
        else
        {
          if (count % 2 == 1) return seq[i - 1];
          else count = 1;
        }
      }

      if (count % 2 == 1) return seq[seq.Length - 1];

      return -1;
    }
  }
}
