def find_uniq(arr):
    a = arr[0]
    count_a = 1
    b = None
    count_b = 0

    for e in arr[1:]:
        if e != a:
            count_b += 1
            b = e
        else:
            count_a += 1
    if count_a < count_b:
        return a
    else:
        return b
