using System;
using System.Collections.Generic;

public class SqInRect {
  public static List<int> sqInRect(int lng, int wdth) {
    if (lng == wdth) return null;

    List<int> result = new List<int>();

    while (lng > 0 && wdth > 0) {
      if (lng < wdth) {
        int a = lng;
        result.Add(a);
        lng = wdth - a;
        wdth = a;
      } else {
        int a = wdth;
        result.Add(a);
        wdth = lng - a;
        lng = a;
      }
    }

    return result;
  }
}
