unit Kata;

interface

function IsPangram (s: string): boolean;

implementation

function IsPangram (s: string): boolean;
var
  i: integer;
  stringSize: integer;
  letters: integer = 0;
begin
  s := LowerCase(s);
  stringSize := Length(s);

  for i := 1 to stringSize do
  begin
    if (s[i] < 'a') or (s[i] > 'z') then
      continue;

    letters := letters or (1 << (Ord(s[i]) - Ord('a')));
  end;

  IsPangram := letters = (1 << 26) - 1;
end;

end.
