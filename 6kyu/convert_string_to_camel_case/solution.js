function toCamelCase(str){
  if (str.indexOf('-') != -1) {
    str = str.split('-');
  } else {
    str = str.split('_');
  }

  for (let i = 1; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].substring(1);
  }

  return str.join('');
}
