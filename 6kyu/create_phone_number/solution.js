function createPhoneNumber(numbers) {
  let result = '(';
  for (let i = 0; i < 10; i++) {
    result += numbers[i];
    if (i == 2) {
      result += ') ';
    } else if (i == 5) {
      result += '-';
    }
  }
  return result;
}
