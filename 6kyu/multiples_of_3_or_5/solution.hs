module MultiplesOf3And5 where

sumOfMultiples :: Integer -> Integer -> Integer
sumOfMultiples n upTo = div ((div (upTo - 1) n) * (n + (upTo - 1 - mod (upTo - 1) n))) 2

solution :: Integer -> Integer
solution number = sumOfMultiples 3 number + sumOfMultiples 5 number - sumOfMultiples 15 number
