func diamond(_ size: Int) -> String? {
  if size < 0 || size % 2 == 0 {
    return nil
  }

  var result = ""
  var (count, diff) = (1, 2)

  while count > 0 {
    print(count, diff)
    result += String(repeating: " ", count: (size - count) / 2)
    result += String(repeating: "*", count: count)
    result += "\n"

    if count == size {
      diff = -2
    }

    count += diff
  }

  return result
}
