function deleteNth(arr, n) {
    const freqs = {};
    const result = [];

    for (const element of arr) {
        if (!freqs[element]) {
            freqs[element] = 1;
        } else if (freqs[element] < n) {
            freqs[element]++;
        } else {
            continue;
        }
        result.push(element);
    }

    return result;
}
