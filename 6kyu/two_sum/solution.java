import java.util.Map;
import java.util.HashMap;

public class Solution {
    public static int[] twoSum(int[] numbers, int target) {
        Map<Integer, Integer> encountered = new HashMap<>();

        for (int i = 0; i < numbers.length; i++) {
            Integer other = encountered.get(target - numbers[i]);
            if (other != null) {
                return new int[] {other, i};
            }
            encountered.put(numbers[i], i);
        }

        return null; // Do your magic!
    }
}
