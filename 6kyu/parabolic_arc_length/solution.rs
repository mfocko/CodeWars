fn len_curve_of<F>(f: F, from: f64, to: f64, n: i32) -> f64
where
    F: Fn(f64) -> f64,
{
    let dx = (to - from) / n as f64;
    let dx2 = dx * dx;

    (0..n)
        .map(|i| {
            let left = f(from + i as f64 * dx);
            let right = f(from + (i + 1) as f64 * dx);

            (dx2 + (left - right).abs().powf(2.0)).sqrt()
        })
        .sum()
}

pub fn len_curve(n: i32) -> f64 {
    len_curve_of(|x| x * x, 0.0, 1.0, n)
}
