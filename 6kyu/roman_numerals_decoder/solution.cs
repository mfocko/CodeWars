using System;
using System.Collections.Generic;

public class RomanDecode {
    private static Dictionary<char, int> values = new Dictionary<char, int>() {
        { 'I', 1 },
        { 'V', 5 },
        { 'X', 10 },
        { 'L', 50 },
        { 'C', 100 },
        { 'D', 500 },
        { 'M', 1000 }
    };

    public static int Solution(string roman) {
        int result = 0;
        var i = roman.Length - 1;
        while (i > 0) {
            var val1 = values[roman[i]];
            var val2 = values[roman[i - 1]];
            if (val1 > val2) {
                result += val1 - val2;
                i -= 2;
            } else {
                result += val1;
                i--;
            }
            Console.WriteLine(result);
        }
        if (i > -1) {
            result += values[roman[i]];
        }
        return result;
    }
}
