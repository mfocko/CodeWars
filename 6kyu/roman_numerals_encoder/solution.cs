using System;
using System.Collections.Generic;
using System.Linq;

public class RomanConvert
{
  private static Dictionary<int, string> values = new Dictionary<int, string>() {
    {    1, "I"  },
    {    4, "IV" },
    {    5, "V"  },
    {    9, "IX" },
    {   10, "X"  },
    {   40, "XL" },
    {   50, "L"  },
    {   90, "XC" },
    {  100, "C"  },
    {  400, "CD" },
    {  500, "D"  },
    {  900, "CM" },
    { 1000, "M"  }
  };

  public static string Solution(int n)
  {
    var result = "";

    var keys = values.Keys.Reverse();

    foreach (var key in keys) {
      while (n / key > 0) {
        result += values[key];
        n -= key;
      }
    }

    return result;
  }
}
