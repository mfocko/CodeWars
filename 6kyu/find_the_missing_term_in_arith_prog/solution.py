def find_missing(sequence):
    # Relation => a_n - a_(n-1) = d

    diffs = []

    for i in range(len(sequence) - 1):
        diffs.append(sequence[i + 1] - sequence[i])

    max_diff = diffs[0]

    for index, diff in enumerate(diffs[1:], 1):
        if abs(diff) > abs(max_diff):
            return sequence[index] + max_diff
        elif abs(max_diff) > abs(diff):
            return sequence[0] + diff

    raise ValueError()
