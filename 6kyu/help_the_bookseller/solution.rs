use std::collections::HashMap;

fn stock_list(list_art: Vec<&str>, list_cat: Vec<&str>) -> String {
    if list_art.is_empty() || list_cat.is_empty() {
        return "".to_string();
    }

    let mut map: HashMap<&str, i32> = HashMap::new();

    // Initialize with zero values
    for cat in &list_cat {
        map.insert(cat, 0);
    }

    // Iterate through art
    for art in &list_art {
        for cat in &list_cat {
            if art.starts_with(cat) {
                let v: Vec<&str> = art.split_terminator(' ').collect();
                let count: i32 = v[1].parse().unwrap();
                map.insert(cat, map[cat] + count);
            }
        }
    }

    // Get result string
    list_cat
        .iter()
        .map(
            |category| format!("({} : {})", category, map[category])
        )
        .collect::<Vec<String>>()
        .as_slice()
        .join(" - ")
}
