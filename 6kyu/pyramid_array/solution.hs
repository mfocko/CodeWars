module Kata where

pyramid :: Int -> [[Int]]
pyramid n = [[1 | _ <- [1..i]] | i <- [1..n]]
