#include <stdio.h>

int is_valid_ip(const char * addr) {
    int seg[4];

    if (sscanf(addr, "%d.%d.%d.%d", &seg[0], &seg[1], &seg[2], &seg[3]) != 4) return 0;

    for (size_t i = 0; i < 4; i++) if (seg[i] <= 0 || seg[i] > 255) return 0;

    int count = 0;

    for (size_t i = 0; addr[i] != '\0'; i++)
    {
        if (addr[i] == '.') count++;
        if ((addr[i] == '.' && addr[i + 1] == '0') || addr[i] == ' ') return 0;
    }

    if (count != 3) return 0;

    return 1;
}
