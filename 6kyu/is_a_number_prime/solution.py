import math

def is_prime(num):
  if num <= 1:
      return False
  elif num == 2:
      return True
  elif num % 2 == 0:
      return False

  bound = int(math.ceil(math.sqrt(num)))

  for div in range(3, bound + 1, 2):
      if num % div == 0:
          return False
  return True
