from functools import reduce

def high(x):
    x = x.split()
    h = reduce(lambda total, char: total + ord(char) - 96, x[0], 0)
    hw = x[0]

    for word in x[1:]:
        total = reduce(lambda total, char: total + ord(char) - 96, word, 0)
        if total > h:
            h = total
            hw = word
    return hw
