def solution(s):
    if len(s) == 0:
        return []
    elif len(s) % 2 == 1:
        s += "_"
    arr = []
    index = -1

    for i, letter in enumerate(s):
        if i % 2 == 0:
            index += 1
            arr.append(letter)
        else:
            arr[index] += letter

    return arr
