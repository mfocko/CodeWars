def fat_fingers(string):
    if string == "":
        return ""

    result = ""
    caps = False

    for letter in string[0:]:
        if letter == 'A' or letter == 'a':
            caps = not caps
        elif caps:
            if letter.isupper():
                result += letter.lower()
            else:
                result += letter.upper()
        else:
            result += letter

    return result
