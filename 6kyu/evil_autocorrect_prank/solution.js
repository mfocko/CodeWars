const REGEX = new RegExp("(^|[^a-z]+)(u|you+)($|[^a-z]+)", "gi")

function autocorrect(input){
  var result = input;

  do {
    input = result;
    result = result.replace(REGEX, "$1your sister$3");
  } while (result != input);

  return result;
}
