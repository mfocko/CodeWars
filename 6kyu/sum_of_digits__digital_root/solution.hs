module DigitalRoot where

digitalRoot :: Integral a => a -> a
digitalRoot 0 = 0
digitalRoot n = if n `mod` 9 == 0 then 9 else n `mod` 9
