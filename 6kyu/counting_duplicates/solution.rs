use std::collections::HashMap;

fn count_duplicates(text: &str) -> u32 {
    let mut counts: HashMap<char, i32> = HashMap::new();

    for character in text.to_lowercase().chars() {
        match counts.get_mut(&character) {
            None => {
                counts.insert(character, 1);
                {}
            },
            Some(x) => *x += 1
        }
    }

    counts
        .iter()
        .fold(0, |total, (_, &count)|
            match count > 1 {
                true => total + 1,
                _    => total
            }
        )
}
