function trickOrTreat(children,candies){
  if (candies.length != children) {
    return "Trick or treat!";
  }

  let lastCount = -1;
  for (let packet of candies) {
    let count = 0;
    for (let e of packet) {
      if (e == "bomb") {
        return "Trick or treat!";
      } else if (e == "candy") {
        count++;
      }
    }

    if (lastCount == -1) {
      lastCount = count;
    }

    if (count < 2 || lastCount != count) {
      return "Trick or treat!";
    }
  }

  return "Thank you, strange uncle!";
}
