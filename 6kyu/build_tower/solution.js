String.prototype.centerJustify = function(length, char) {
  let i = 0;
  let str = this;
  let toggle = true;
  while (i + this.length < length) {
    i++;
    if (toggle)
      str = str + char;
    else
      str = char + str;
    toggle = !toggle;
  }
  return str;
}

function towerBuilder(nFloors) {
  let floor = '*';
  const width = nFloors * 2 - 1;
  let result = [];
  for (let i = 0, count = 1; i < nFloors; i++, count += 2) {
    result.push(floor.centerJustify(width, ' '));
    floor += '**';
  }
  return result;
}
