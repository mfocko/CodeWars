fun getSum(n: Int, p: Int, l: Int): Int {
    var result = 0
    var runningN = n

    for (q in p + l - 1 downTo p) {
        result += Math.pow(runningN.rem(10).toDouble(), q.toDouble()).toInt()
        runningN /= 10
    }

    return result
}

fun digPow(n: Int, p: Int): Int {
    val sum = getSum(n, p, n.toString().length)
    return if (sum.rem(n) == 0) sum.div(n) else -1
}
