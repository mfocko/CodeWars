using System.Text.RegularExpressions;

public static class Kata
{
  public static int CountSmileys(string[] smileys)
  {
    var count = 0;

    var regex = new Regex(@"[:;][-~]?[D\)]");

    foreach (string smiley in smileys) {
      if (regex.Match(smiley).Success) count++;
    }

    return count;
  }
}
