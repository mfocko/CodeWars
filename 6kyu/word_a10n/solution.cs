using System;

public class Abbreviator
{
  public static string Abbreviate(string input)
  {
    string result = "", temp = "";

    for (int i = 0; i < input.Length; i++)
    {
      if (Char.IsLetter(input[i]))
        temp += input[i];
      else
      {
        if (temp.Length <= 3) result += temp + input[i];
        else
        {
          result += temp[0] + (temp.Length - 2).ToString() + temp[temp.Length - 1] + input[i];
        }
        temp = "";
      }
    }

    if (temp.Length <= 3) result += temp;
    else
    {
      result += temp[0] + (temp.Length - 2).ToString() + temp[temp.Length - 1];
    }

    return result;
  }
}
