using System;

public class Kata {
  public static bool Narcissistic(int value) {
    var narcissistic = 0;
    var length = (int) Math.Log10(value + 0.5) + 1;
    var copyOfValue = value;

    while (copyOfValue > 0) {
      narcissistic += (int) Math.Pow(copyOfValue % 10, length);
      copyOfValue /= 10;
      if (narcissistic > value) {
        return false;
      }
    }

    return narcissistic == value;
  }
}
