module Codewars.Kata.PascalsTriangle where

pascalsRow :: Int -> [Int]
pascalsRow n = scanl (\nCk k -> (nCk * (n - k)) `div` (k + 1)) 1 [0..n - 1]

pascalsTriangle :: Int -> [Int]
pascalsTriangle n = concatMap pascalsRow [0..n - 1]
