def solution(string)
  string.chars.map { |c|
    if c == c.upcase
      " " + c
    else
      c
    end
  }.join
end
