#include <map>
#include <set>

class Automaton
{
  std::set<int> F{2};
  const int initial_state = 1;

  std::map<int, std::map<char, int>> transitions{
    {1, std::map<char, int>{
        {'0', 1}, {'1', 2}
      }
    },
    {2, std::map<char, int>{
        {'0', 3}, {'1', 2}
      }
    },
    {3, std::map<char, int>{
        {'0', 2}, {'1', 2}
      }
    }
  };

public:
  Automaton() {}

  bool read_commands(const std::vector<char>& commands) {
    int state = initial_state;

    for (const char c : commands) {
      state = transitions[state][c];
    }

    return F.find(state) != F.end();
  }
};
