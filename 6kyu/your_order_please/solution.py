def order(sentence):
  arr = sentence.split(' ')
  result = ''
  count = 1
  for i in range(len(arr)):
      for j in range(len(arr)):
          if str(count) in arr[j]:
              result += arr[j] + ' '
              del arr[j]
              count += 1
              break
  return result.strip()
