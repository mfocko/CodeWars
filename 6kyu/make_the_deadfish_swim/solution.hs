module Kata.Deadfish (parse) where

parse :: String -> [Int]
parse = snd . foldl exec (0, [])
  where exec (value, output) cmd = case cmd of
          'i' -> (value + 1, output)
          'd' -> (value - 1, output)
          's' -> (value * value, output)
          'o' -> (value, output ++ [value])
          _ -> (value, output)
