using System.Collections.Generic;
using System.Linq;

public class Kata {
    public static int GetLengthOfMissingArray(object[][] arrayOfArrays) {
        if (arrayOfArrays == null || arrayOfArrays.GetLength(0) < 2) return 0;
        var lengths = new HashSet<int>();

        foreach (var array in arrayOfArrays) {
            if (array == null) return 0;
            var size = array.GetLength(0);
            if (size == 0) return 0;
            else lengths.Add(size);
        }

        var max = lengths.Max();
        var min = lengths.Min();

        return (int) (0.5 * (arrayOfArrays.GetLength(0) + 1) * (max + min) - lengths.Sum());
    }
}
