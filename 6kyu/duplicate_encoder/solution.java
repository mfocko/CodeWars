public class DuplicateEncoder {
  static String encode(String word) {
    word = word.toLowerCase();
    String result = "";
    for (int i = 0; i < word.length(); i++) {
      char character = word.charAt(i);
      if (word.indexOf(character) != word.lastIndexOf(character)) {
        result += ")";
      } else {
        result += "(";
      }
    }
    return result;
  }
}
