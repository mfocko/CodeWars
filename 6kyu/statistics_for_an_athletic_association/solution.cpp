#include <algorithm>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

class Stat
{
    unsigned int h;
    unsigned int m;
    unsigned int s;

  public:
    static std::string stat(const std::string &strg);

    Stat(const std::string &strg);
    Stat(unsigned int seconds);
    unsigned int get_seconds() const;
    friend std::ostream &operator<<(std::ostream &out, const Stat &s);
    bool operator<(const Stat &s);
};

std::string Stat::stat(const std::string &strg)
{
    std::vector<Stat> stats;
    std::string buffer;

    for (const char &c : strg)
    {
        if (c == ' ')
            continue;
        else if (c == ',')
        {
            stats.push_back(Stat(buffer));
            buffer = "";
        }
        else
            buffer += c;
    }

    stats.push_back(Stat(buffer));

    std::sort(stats.begin(), stats.end(), [](Stat a, Stat b) {
        return a.get_seconds() < b.get_seconds();
    });

    unsigned int seconds;

    seconds = stats.back().get_seconds() - stats.front().get_seconds();
    Stat range(seconds);

    seconds = std::accumulate(stats.begin(), stats.end(), 0u,
                              [](unsigned int r, Stat s) {
                                  return r + s.get_seconds();
                              });

    Stat average(seconds / stats.size());

    if (stats.size() % 2 == 1)
    {
        seconds = stats[stats.size() / 2].get_seconds();
    }
    else
    {
        size_t index = stats.size() / 2;
        seconds = (stats[index].get_seconds() + stats[index - 1].get_seconds()) / 2;
    }
    Stat median(seconds);

    std::stringstream result;

    result << "Range: " << range << " Average: " << average << " Median: " << median;

    return result.str();
}

Stat::Stat(const std::string &strg)
{
    char delimiter;
    std::stringstream ss(strg);
    ss >> this->h >> delimiter >> this->m >> delimiter >> this->s;
}

Stat::Stat(unsigned int seconds)
{
    this->h = seconds / 3600;
    seconds %= 3600;
    this->m = seconds / 60;
    this->s = seconds % 60;
}

unsigned int Stat::get_seconds() const
{
    return this->h * 3600 + this->m * 60 + this->s;
}

std::ostream &operator<<(std::ostream &out, const Stat &s)
{
    out << std::setfill('0') << std::setw(2) << s.h << "|";
    out << std::setfill('0') << std::setw(2) << s.m << "|";
    out << std::setfill('0') << std::setw(2) << s.s;
    return out;
}
