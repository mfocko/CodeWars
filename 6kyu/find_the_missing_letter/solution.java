public class Kata {
  public static char findMissingLetter(char[] array) {
    int last = (int)array[0];
    for (int i = 1; i < array.length; i++)
        if ((int)array[i] != last + 1) {
            return (char) (last + 1);
        } else {
            last++;
        }
    return (char) -1;
  }
}
