using System;
using System.Linq;

public class LOTTO {
  private static Random r = new Random();

  public static int[] NumberGenerator() {
    var result = new int[7];
    result[6] = 50;

    for (var i = 0; i < 6; i++) {
      var number = 0;
      do {
        number = r.Next(1, 50);
      } while (result.Contains(number));
      result[i] = number;
    }
    Array.Sort(result);
    result[6] = r.Next(0, 10);

    return result;
  }

  public static int CheckForWinningCategory(int[] checkCombination, int[] winningCombination) {
    var count = 0;
    var superzahl = checkCombination.Last() == winningCombination.Last();

    for (var i = 0; i < 6; i++) {
      var index = Array.IndexOf(checkCombination, winningCombination[i]);
      if (index != -1 && index != 6) count++;
    }

    var result = -1;

    switch (count) {
      case 6:
        result = 2;
        break;
      case 5:
        result = 4;
        break;
      case 4:
        result = 6;
        break;
      case 3:
        result = 8;
        break;
    }

    if (count == 2 && superzahl) result = 9;
    else if (superzahl && result != -1)  result--;

    return result;
  }
}
