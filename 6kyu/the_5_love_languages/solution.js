function loveLanguage(partner, weeks) {
  const options = ["words", "acts", "gifts", "time", "touch"];
  const tries = 7 * weeks;
  
  let counters = {
    "words": 0,
    "acts": 0,
    "gifts": 0,
    "time": 0,
    "touch": 0,
  };
  
  for (let i = 0; i < tries; ++i) {
    const choice = options[i % options.length];
    
    if (partner.response(choice) == "positive") {
      counters[choice]++;
    }
  }
  
  let best = 0;
  let bestChoice = null;
  for (let choice of options) {
    if (counters[choice] > best) {
      best = counters[choice];
      bestChoice = choice;
    }
  }
  
  return bestChoice;
}
