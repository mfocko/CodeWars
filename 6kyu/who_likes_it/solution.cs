using System;

public static class Kata
{
    public static string Likes(string[] name)
    {
        var prefix = "";
        switch (name.Length) {
          case 0:
            prefix = "no one";
            break;
          case 1:
            prefix = name[0];
            break;
          case 2:
            prefix = String.Join(" and ", name);
            break;
          case 3:
            prefix = String.Join(", ", name, 0, 2) + " and " + name[2];
            break;
          default:
            prefix = String.Join(", ", name, 0, 2) + String.Format(" and {0} others", name.Length - 2);
            break;
        }

        if (name.Length <= 1) return prefix + " likes this";
        else return prefix + " like this";
    }
}
