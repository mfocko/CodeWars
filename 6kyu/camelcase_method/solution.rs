fn camel_case(str: &str) -> String {
    let mut result = String::from("");

    let mut was_space: bool = true;
    for character in str.chars() {
        if character.is_whitespace() {
            was_space = true;
        } else if character.is_alphabetic() {
            if was_space {
                result += &character.to_uppercase().to_string();
                was_space = false;
            } else {
                result += &character.to_lowercase().to_string();
            }
        }
    }

    result.to_string()
}
