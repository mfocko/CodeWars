package ball

val G = 9.81

fun maxBall(v0: Int): Int {
    val v = v0 * 5 / 18.0
    val maxT = v * 10 / G

    return (0..maxT.toInt() + 1).maxBy {
        v * it / 10.0 - 0.5 * G * it * it / 100.0
    } ?: 0
}
