module Codewars.Kata.Xor where

xor :: Bool -> Bool -> Bool
xor = (!=)
