#include <stdbool.h>

bool is_uppercase(const char *source)
{
    for (int i = 0; source[i] != '\0'; i++)
    {
        if (source[i] >= 'a' && source[i] <= 'z')
            return false;
    }

    return true;
}
