using System;
using System.Collections.Generic;

namespace Solution
{
  class Digitizer
  {
    public static long[] Digitize(long n)
    {
      long[] arr = new long[(int) Math.Log10(n + 0.5) + 1];

      long n_tmp = n;
      for (int i = 0, i_max = arr.Length; i < i_max; i++)
      {
        arr[i] = n_tmp % 10;
        n_tmp /= 10;
      }
      return arr;
    }
  }
}
