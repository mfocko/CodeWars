def make_upper_case(s):
    result = ""
    for char in s:
        result += char.upper()
    return result
