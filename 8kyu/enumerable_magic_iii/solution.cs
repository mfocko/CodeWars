public class Kata
{
  public static bool Include(int[] arr, int item)
  {
    foreach (var e in arr) {
      if (e == item) return true;
    }

    return false;
  }
}
