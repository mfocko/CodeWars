using System;

public class Kata
{
  public static int[] SquareOrSquareRoot(int[] array)
  {
    int[] new_array = new int[array.Length];

    for (int i = 0, max = array.Length; i < max; i++)
    {
      double root = Math.Sqrt(array[i]);
      if (root - (int) root <= double.Epsilon) new_array[i] = (int) root;
      else new_array[i] = array[i] * array[i];
    }

    return new_array;
  }
}
