sing namespace std;

int count_sheep(vector<bool> arr)
{
    int count = 0;
    for (const bool e : arr)
        if (e) count++;
    return count;
}
