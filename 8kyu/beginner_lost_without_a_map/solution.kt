fun maps(x: IntArray): IntArray = x.map { 2 * it }.toIntArray()
