unit GrashopperSummation;

{$mode objfpc}{$H+}

interface

function Summation(const N: Integer): Integer;

implementation

function Summation(const N: Integer): Integer;
begin
  Result := N*(N + 1) div 2;
end;

end.
