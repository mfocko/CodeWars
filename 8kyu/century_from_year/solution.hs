module Century where

century :: Int -> Int
century year = case year `mod` 100 of
                0 ->     year `div` 100
                _ -> 1 + year `div` 100
