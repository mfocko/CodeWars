fun loveFun(flowerA: Int, flowerB: Int): Boolean = flowerA.xor(flowerB).and(1) != 0
