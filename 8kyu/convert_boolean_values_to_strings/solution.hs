module YesNo where

boolToWord :: Bool -> String
boolToWord True = "Yes"
boolToWord False = "No"
