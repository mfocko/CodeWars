function squareSum(numbers) {
  let result = 0;

  for (let x of numbers) {
    result += x * x;
  }

  return result;
}
