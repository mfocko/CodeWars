using System;
public class Kata {
  public static string FindNeedle(object[] haystack) {
    var index = 0;

    foreach (var o in haystack) {
      //if (o is string s && s == "needle") break;
      if (o == null);
      else if (o.ToString() == "needle") break;
      index++;
    }

    return $"found the needle at position {index}";
  }
}
