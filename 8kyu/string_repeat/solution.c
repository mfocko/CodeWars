#include <string.h>

char *repeat_str(size_t count, const char *src) {
  char* result = (char *) malloc(strlen(src) * count);

  size_t upper = count * strlen(src);
  size_t len = strlen(src);

  for (size_t i = 0; i < upper; i += len)
    strcpy(&result[i], src);

  return result;
}
