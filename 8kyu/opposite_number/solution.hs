module Codewars.Kata.Opposite where

opposite :: Num a => a -> a
opposite num = - num
