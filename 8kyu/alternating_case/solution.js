String.prototype.toAlternatingCase = function () {
    // Define your method here :)
    let result = "";

    for (let i = 0; i < this.length; i++) {
        if (this[i].toLowerCase() === this[i]) {
            result += this[i].toUpperCase();
        } else {
            result += this[i].toLowerCase();
        }
    }

    return result;
}
