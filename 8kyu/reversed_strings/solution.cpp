#include <string>
using namespace std;

string reverseString (string str)
{
  string res = "";
  for (int i = str.size() - 1; i >= 0; i--) res += str[i];
  return res;
}
