void fakeBin(const char *digits, char *buffer) {
  char * digit = digits;
  char * new_digit = buffer;

  while (*digit != '\0') {
    if (*digit >= '5') {
      *new_digit = '1';
    } else {
      *new_digit = '0';
    }

    digit++;
    new_digit++;
  }
  *new_digit = '\0';
  printf("%s\n", digits);
  printf("%s\n", buffer);
}
