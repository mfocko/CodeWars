def positive_sum(arr):
    return sum(filter(lambda e: e > 0, arr))
