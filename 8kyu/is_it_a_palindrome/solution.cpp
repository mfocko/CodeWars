#include <cctype>
#include <string>

bool isPalindrom (const std::string& str)
{
    if (str.size() == 0) return true;

    auto i = 0;
    auto j = str.size() - 1;

    std::cout << str << std::endl;

    while (i < j)
    {
        if (std::tolower(str[i++]) != std::tolower(str[j--]))
        {
            return false;
        }
    }

    return true;
}
