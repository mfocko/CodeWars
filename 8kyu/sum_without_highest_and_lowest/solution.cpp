#include<vector>
using namespace std;

int sum(vector<int> numbers)
{
    if (numbers.size() < 3) return 0;

    int min = numbers[0];
    int max = numbers[0];

    int result = 0;

    for (const int num : numbers)
    {
        if (num < min)
            min = num;
        else if (num > max)
            max = num;

        result += num;
    }

    return result - (min + max);
}
