unit Kata;

interface

function CockroachSpeed(s: Real): Integer;

implementation

function CockroachSpeed(s: Real): Integer;
begin
  Result := Trunc(250 * s / 9);
end;

end.
