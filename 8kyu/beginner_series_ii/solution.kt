fun past(h: Int, m: Int, s: Int): Int = h.times(60).plus(m).times(60).plus(s).times(1000)
