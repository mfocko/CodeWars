namespace Solution {
  using System;
  using System.Collections.Generic;

  public class BattleshipField {
    private enum Direction {
      HORIZONTAL,
      VERTICAL
    }

    private class Ship {
      private int y, x;
      private Direction d;
      private int size;

      public int Size {get => size;}

      public Ship(int y, int x, Direction d, int size) {
        this.y = y;
        this.x = x;
        this.d = d;
        this.size = size;
      }
      public bool IsTouching(Ship b) {
        if (d == Direction.HORIZONTAL) {
          if ((b.y >= this.y - 1 && b.y <= this.y + 1) &&
            (b.x >= this.x - 1 && b.x <= this.x + this.size))
            return true;
          else return false;
        }

        if (d == Direction.VERTICAL) {
          if ((b.x >= this.x - 1 && b.x <= this.x + 1) &&
            (b.y >= this.y - 1 && b.y <= this.y + this.size))
            return true;
          else return false;
        }

        return false;
      }
      public override string ToString() {
        string direction = this.d == Direction.HORIZONTAL ? "horizontal" : "vertical";
        return $"x: {this.x}\ty: {this.y}\tdir: {direction}\tsize: {this.size}";
      }
    }

    private List<Ship> ships;
    private int battleships = 1,
                   cruisers = 2,
                 destroyers = 3,
                 submarines = 4;

    public BattleshipField() {
      this.ships = new List<Ship>();
    }

    private void registerShip(int y, int x, Direction d, int count) {
      this.ships.Add(new Ship(y, x, d, count));
      switch (count) {
        case 1:
          this.submarines--;
          break;
        case 2:
          this.destroyers--;
          break;
        case 3:
          this.cruisers--;
          break;
        case 4:
          this.battleships--;
          break;
      }
    }

    private bool areTouching() {
     for (var i = 0; i < this.ships.Count - 1; i++)
        for (var j = i + 1; j < this.ships.Count; j++)
          if (this.ships[i].IsTouching(this.ships[j])) return true;
      return false;
    }

    public bool IsValidField() {
      if (this.submarines != 0 || this.destroyers != 0 ||
          this.cruisers != 0 || this.battleships != 0)
        return false;
      else if (this.areTouching())
        return false;
      return true;
    }

    public static bool ValidateBattlefield(int[,] field) {
      // Write your magic here
      var validator = new BattleshipField();
      int count = 0;

      for (int y = 0, rows = field.GetLength(0); y < rows; y++) {
        for (int x = 0, cols = field.GetLength(0); x < cols; x++) {
          if (field[y, x] == 1) {
            // check for vertical ship
            if (count == 0) {
              if (y < rows - 1 && field[y + 1, x] == 1) {
                for (int dy = 0; y + dy < rows; dy++) {
                  if (field[y + dy, x] == 1) {
                    field[y + dy, x] = 0;
                    count++;
                  } else break;
                }

                if (count > 1) {
                  validator.registerShip(y, x, Direction.VERTICAL, count);
                  count = 0;
                  continue;
                }
              }
            }
            field[y, x] = 0;
            count++;
          } else if (field[y, x] == 0 && count > 0) {
            validator.registerShip(y, x - count, Direction.HORIZONTAL, count);
            count = 0;
          }
        }
      }
      return validator.IsValidField();
    }
  }
}
