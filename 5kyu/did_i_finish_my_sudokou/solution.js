function checkSet(set) {
    if (set.size != 9) {
        return false;
    } else {
        for (let num = 1; num <= 9; num++) {
            if (!set.has(num)) {
                return false;
            }
        }
    }

    return true;
}

function doneOrNot(board) {
    const positive = "Finished!";
    const negative = "Try again!";

    // Check rows
    for (let row of board) {
        row = new Set(row);
        if (!checkSet(row)) {
            return negative;
        }
    }

    // Check columns
    for (let col = 0; col < 9; col++) {
        // Construct set for a column
        let column = new Set();
        for (let i = 0; i < 9; i++) {
            column.add(board[i][col]);
        }

        if (!checkSet(column)) {
            return negative;
        }
    }

    // Check boxes
    for (let i = 0; i < 9; i += 3) {
        for (let j = 0; j < 9; j += 3) {
            // Construct set of a box
            let box = new Set();
            for (let k = 0; k < 3; k++) {
                box.add(board[i + k][j]).add(board[i][j + k]).add(board[i + k][j + k]);
            }
            box.add(board[i + 2][j + 1]).add(board[i + 1][j + 2]);

            if (!checkSet(box)) {
                return negative;
            }
        }
    }

    return positive;
}
