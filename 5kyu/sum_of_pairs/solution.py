def sum_pairs(ints, s):
  passed = {}

  for num in ints:
    other = s - num
    if other in passed:
      return [other, num]
    else:
      passed[num] = True

  return None
