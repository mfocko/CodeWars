#include <vector>
#include <cmath>

class RemovedNumbers
{
    static long long get_lower_bound(long long n);
    static long long get_complement(long long ax, long long n);

  public:
    static std::vector<std::vector<long long>> removNb(long long n);
};

long long RemovedNumbers::get_lower_bound(long long n)
{
    return (long long)ceil((2 * n - n * n - n) / (-2 * n - 2.0));
}

long long RemovedNumbers::get_complement(long long ax, long long n)
{
    auto result = (0.5 * (n * n + n) - ax) / (ax + 1);

    if (floor(result) == result)
    {
        return (long long)result;
    }
    else
    {
        return 0;
    }
}

std::vector<std::vector<long long>> RemovedNumbers::removNb(long long n)
{
    std::vector<std::vector<long long>> result;

    for (long long ax = get_lower_bound(n); ax <= n; ax++)
    {
        auto complement = get_complement(ax, n);
        if (complement != 0)
        {
            result.push_back({ax, complement});
        }
    }

    return result;
}
