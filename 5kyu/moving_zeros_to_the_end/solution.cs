using System.Collections.Generic;
using System.Linq;

public class Kata {
  public static int[] MoveZeroes(int[] arr) {
    var length = arr.GetLength(0);

    var new_arr = arr.Where(x => x != 0).ToList();
    for (var i = length - new_arr.Count(); i > 0; i--) new_arr.Add(0);

    return new_arr.ToArray();
  }
}
