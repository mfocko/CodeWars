fibonacci_memory = {}

def fibonacci(n):
    if n in [0, 1]:
        return n

    if n not in fibonacci_memory:
        value = fibonacci(n - 1) + fibonacci(n - 2)
        fibonacci_memory[n] = value
        return value
    else:
        return fibonacci_memory[n]
