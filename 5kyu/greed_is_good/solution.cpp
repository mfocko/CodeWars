#include <map>
#include <tuple>
#include <vector>

std::map<int, int> process_dice(const std::vector<int>& dice) {
  std::map<int, int> freqs;

  for (auto val : dice) {
    freqs[val]++;
  }

  return freqs;
}

int score(const std::vector<int>& dice) {
  const std::map<std::tuple<int, int>, int> options{
    {{3, 1}, 1000},
    {{3, 6}, 600},
    {{3, 5}, 500},
    {{3, 4}, 400},
    {{3, 3}, 300},
    {{3, 2}, 200}
  };

  auto counts = process_dice(dice);
  auto total = 0;

  for (auto has_changed = true; has_changed;) {
    has_changed = false;

    for (const auto& [option, points] : options) {
      const auto& [count, die] = option;

      if (counts[die] >= count) {
        counts[die] -= count;
        total += points;

        has_changed = true;
        break;
      }
    }
  }

  total += counts[1] * 100;
  total += counts[5] * 50;

  return total;
}
