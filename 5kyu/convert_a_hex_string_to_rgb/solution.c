#include <stdio.h>

typedef struct {
    int r, g, b;
} rgb;

rgb hex_str_to_rgb(const char *hex_str) {
    rgb result;

    sscanf(hex_str, "#%2x%2x%2x", &result.r, &result.g, &result.b);

    return result;
}
