using System;
using System.Collections.Generic;

public class Scramblies {
    public static bool Scramble(string str1, string str2) {
        var list = new List<char>(str1.ToCharArray());
        list.Sort();

        foreach (char letter in str2) {
            if (list.Contains(letter)) {
                list.Remove(letter);
            } else {
                return false;
            }
        }

        return true;
    }
}
