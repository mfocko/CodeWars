fun getSumOfDivisors(n: Long): Long =
    (1 until Math.sqrt(n.toDouble()).toLong() + 1).reduce {
        acc, e -> acc + if (n.rem(e) == 0.toLong()) e + n.div(e) else 0.toLong()
    }

fun buddy(start: Long, limit: Long): String {
    for (n in start..limit) {
        val m = getSumOfDivisors(n) - 1
        if (m > n && getSumOfDivisors(m) - 1 == n) {
            return "($n $m)"
        }
    }

    return "Nothing"
}
