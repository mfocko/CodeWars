#include <inttypes.h>
#include <stdio.h>

void uint32_to_ip(uint32_t ip, char *output) {
  sprintf(output, "%d.%d.%d.%d", (ip >> 24) & 0xFF, (ip >> 16) & 0xFF, (ip >> 8) & 0xFF, ip & 0xFF);
}
