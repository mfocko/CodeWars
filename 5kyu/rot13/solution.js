function rot13(message) {
  let result = "";

  for (let character of message) {
    let val = character.charCodeAt();

    if (val >= 97 && val <= 122) {
      result += String.fromCharCode(97 + (val - 97 + 13) % 26);
    } else if (val >= 65 && val <= 90) {
      result += String.fromCharCode(65 + (val - 65 + 13) % 26);
    } else {
      result += character;
    }
  }

  return result;
}
