fn parse_ip(ip_str: &str) -> u32 {
    ip_str
    .split(".")
    .map(|octet| octet.parse::<u32>().unwrap())
    .fold(0, |ip, octet| (ip << 8) | octet)
}

pub fn ips_between(start: &str, end: &str) -> u32 {
    parse_ip(end) - parse_ip(start)
}
