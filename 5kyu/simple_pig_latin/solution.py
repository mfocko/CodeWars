def pig_it(text):
    result = ''

    for word in text.split():
        if word in '.?!\',':
            result += word + ' '
        else:
            result += word[1:] + word[0] + 'ay '
    return result.strip()
