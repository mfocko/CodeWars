function pick(bags, scale) {
  if (bags.length == 1) {
    return bags[0];
  }

  let sets = [];
  let sizeOfSet = bags.length / 3;

  for (let i = 0; i < bags.length; i += sizeOfSet) {
    sets.push(bags.slice(i, i + sizeOfSet));
  }

  switch (scale.weigh(sets[0], sets[1])) {
    case -1:
      return pick(sets[0], scale);
      break;
    case 0:
      return pick(sets[2], scale);
      break;
    case 1:
      return pick(sets[1], scale);
      break;
  }
}
