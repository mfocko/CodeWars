module Codewars.Kata.Reduction where
import Codewars.Kata.Reduction.Direction

-- data Direction = North | East | West | South deriving (Eq)

areOpposite :: Direction -> Direction -> Bool
areOpposite North South = True
areOpposite South North = True
areOpposite East West = True
areOpposite West East = True
areOpposite _ _ = False

folding :: Direction -> [Direction] -> [Direction]
folding x [] = [x]
folding x (y:rest) | areOpposite x y = rest
                   | otherwise = x:y:rest

dirReduce :: [Direction] -> [Direction]
dirReduce [] = []
dirReduce directions = foldr folding [] directions
