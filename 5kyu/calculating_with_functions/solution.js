function zero(operand=null)     { return (operand) ? operand(0) : 0; }
function one(operand=null)      { return (operand) ? operand(1) : 1; }
function two(operand=null)      { return (operand) ? operand(2) : 2; }
function three(operand=null)    { return (operand) ? operand(3) : 3; }
function four(operand=null)     { return (operand) ? operand(4) : 4; }
function five(operand=null)     { return (operand) ? operand(5) : 5; }
function six(operand=null)      { return (operand) ? operand(6) : 6; }
function seven(operand=null)    { return (operand) ? operand(7) : 7; }
function eight(operand=null)    { return (operand) ? operand(8) : 8; }
function nine(operand=null)     { return (operand) ? operand(9) : 9; }

function plus(right)            { return (x) => x + right; }
function minus(right)           { return (x) => x - right; }
function times(right)           { return (x) => x * right; }
function dividedBy(right)       { return (x) => Math.floor(x / right); }
