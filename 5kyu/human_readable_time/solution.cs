using System;

public static class TimeFormat
{
    public static string GetReadableTime(int seconds)
    {
        var hours = seconds / 3600;
        seconds %= 3600;
        var minutes = seconds / 60;
        seconds %= 60;

        return String.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
    }
}
