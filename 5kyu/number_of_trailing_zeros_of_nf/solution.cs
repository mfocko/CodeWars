using System;

public static class Kata
{
  public static int TrailingZeros(int n)
  {
    var sum = 0.0;
    var max = Math.Floor(Math.Log(n)/Math.Log(5));

    for (var i = 1; i <= max; i++) {
      sum += Math.Floor(n / Math.Pow(5, i));
    }

    return (int) sum;
  }
}
