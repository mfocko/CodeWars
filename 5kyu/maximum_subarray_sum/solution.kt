fun maxSequence(arr: List<Int>): Int =
    arr.scan(0) { k, it ->
        maxOf(k, 0) + it
    }.maxOrNull() ?: 0
