def digit_sum(n):
    total = 0
    while n > 0:
        total += n % 10
        n //= 10
    return total

class Weight:
    def __init__(self, weight):
        self.weight = weight
        self.digit_sum = digit_sum(weight)

    def __lt__(self, other):
        if self.digit_sum == other.digit_sum:
            return str(self.weight) < str(other.weight)
        return self.digit_sum < other.digit_sum

    def __str__(self):
        return str(self.weight)

def order_weight(strng):
    return ' '.join(map(lambda e: str(e), sorted(map(lambda e: Weight(int(e)), strng.split()))))
