#include <map>

class SnakesLadders
{
  private:
    static std::map<int, int> sls;
    int positions[2];
    int player = 0;
  public:
    SnakesLadders(){
      positions[0] = positions[1] = 0;
    };
    std::string play(int die1, int die2)
    {
      if (positions[0] == 100 || positions[1] == 100) {
        return "Game over!";
      } else {
        positions[player] += die1 + die2;

        if (positions[player] > 100) {
          positions[player] = 100 - positions[player] % 100;
        } else if (positions[player] == 100) {
          return "Player " + std::to_string(player + 1) + " Wins!";
        }

        if (sls.find(positions[player]) != sls.end()) {
          positions[player] = sls[positions[player]];
        }

        int p = player + 1;
        if (die1 != die2) {
          player = (player + 1) % 2;
        }

        return "Player " + std::to_string(p) + " is on square " + std::to_string(positions[p - 1]);
      }
    };
};

std::map<int, int> SnakesLadders::sls = {
    { 2, 38},
    { 7, 14},
    { 8, 31},
    {15, 26},
    {16,  6},
    {21, 42},
    {28, 84},
    {36, 44},
    {46, 25},
    {49, 11},
    {51, 67},
    {62, 19},
    {64, 60},
    {71, 91},
    {74, 53},
    {78, 98},
    {87, 94},
    {89, 68},
    {92, 88},
    {95, 75},
    {99, 80},
};
