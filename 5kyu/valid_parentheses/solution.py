def valid_parentheses(string):
    opened = 0

    for letter in string:
        if letter == '(':
            opened += 1
        elif letter == ')':
            opened -= 1
        if opened < 0:
            return False
    return opened == 0
