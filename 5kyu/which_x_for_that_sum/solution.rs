fn solve_quadratic(a: f64, b: f64, c: f64) -> Option<(f64, f64)> {
    let d = b * b - 4.0 * a * c;

    if d < 0.0 {
        None
    } else {
        Some(((-b + d.sqrt()) / (2.0 * a), (-b - d.sqrt()) / (2.0 * a)))
    }
}

pub fn solve(m: f64) -> f64 {
    match solve_quadratic(m, -2.0 * m - 1.0, m) {
        Some((left, right)) => {
            if left > 0.0 && left < 1.0 {
                left
            } else if right > 0.0 && right < 1.0 {
                right
            } else {
                0.0
            }
        }
        None => 0.0,
    }
}
