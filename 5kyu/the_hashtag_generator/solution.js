function generateHashtag (str) {
  const result = str.split(/\s+/igm).reduce((accum, word) =>
    accum + word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()
  , "#");

  return (result.length > 140 || result == "#") ? false : result;
}
