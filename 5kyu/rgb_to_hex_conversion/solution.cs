public class Kata
{
  public static int Constrain(int val, int min, int max) {
    if (val < min) return min;
    else if (val > max) return max;
    else return val;
  }

  public static string Rgb(int r, int g, int b)
  {
    r = Constrain(r, 0, 255);
    g = Constrain(g, 0, 255);
    b = Constrain(b, 0, 255);
    return $"{r,2:X2}{g,2:X2}{b,2:X2}";
  }
}
