using System;

public class Fracts {
    public static string convertFrac(long[,] lst)
    {
        if (lst.GetLength(0) < 1) return "";
        long denominator = lst[0, 1];

        string result = "";

        for (int i = 0; i < lst.GetLength(0); i++) denominator = LeastCommonMultiple(denominator, lst[i, 1]);

        for (int i = 0; i < lst.GetLength(0); i++)
        {
            long factor = denominator / lst[i, 1];
            result += String.Format("({0},{1})", lst[i, 0] * factor, lst[i, 1] * factor);
        }

        return result;
    }

    public static long GreatestCommonDivisor(long a, long b)
    {
        if (b == 0) return a;
        else return GreatestCommonDivisor(b, a % b);
    }

    public static long LeastCommonMultiple(long a, long b)
    {
        return a * b / GreatestCommonDivisor(a, b);
    }
}
