function anagrams(word, words) {
  word = word.split('');
  word.sort();
  word = word.join('');

  let result = [];
  for (let testedWord of words) {
    twSorted = testedWord.split('');
    twSorted.sort();
    if (word == twSorted.join(''))
      result.push(testedWord);
  }

  return result;
}
