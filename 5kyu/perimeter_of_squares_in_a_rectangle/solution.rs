use std::vec::Vec;

fn fibonacci(n: u64) -> Vec<u64> {
    match n {
        0 => vec![1],
        1 => vec![1, 1],
        _ => {
            let mut result = vec![1, 1];

            for i in 2..(n as usize + 1) {
                result.push(result[i - 1] + result[i - 2]);
            }

            result
        }
    }
}

pub fn perimeter(n: u64) -> u64 {
    fibonacci(n).iter().fold(0, |acc, x| acc + 4 * x)
}
