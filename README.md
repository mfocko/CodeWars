# CodeWars

## 1 kyu

## 2 kyu

## 3 kyu

### C#

- [Screen Locking Patterns](https://www.codewars.com/kata/585894545a8a07255e0002f1) - [solution](3kyu/screen_locking_patterns)
- [Binomial Expansion](https://www.codewars.com/kata/540d0fdd3b6532e5c3000b5b) - [solution](3kyu/binomial_expansion)
- [Battleship field validator](https://www.codewars.com/kata/52bb6539a4cf1b12d90005b7) - [solution](3kyu/battleship_field_validator)

## 4 kyu

### Rust

- [Sum by Factors](https://www.codewars.com/kata/54d496788776e49e6b00052f) - [solution](4kyu/sum_by_factors)
- [Factorial tail](https://www.codewars.com/kata/55c4eb777e07c13528000021) - [solution](4kyu/factorial_tail)

### C#

- [Magnet particules in boxes](https://www.codewars.com/kata/56c04261c3fcf33f2d000534) - [solution](4kyu/magnet_particules_in_boxes)
- [The observed PIN](https://www.codewars.com/kata/5263c6999e0f40dee200059d) - [solution](4kyu/the_observed_pin)
- [Next bigger number with the same digits](https://www.codewars.com/kata/55983863da40caa2c900004e) - [solution](4kyu/next_bigger_with_same_digits)
- [Snail](https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1) - [solution](4kyu/snail)
- [Pyramid Slide Down](https://www.codewars.com/kata/551f23362ff852e2ab000037) - [solution](4kyu/pyramid_slide_down)
- [Human readable duration format](https://www.codewars.com/kata/52742f58faf5485cae000b9a) - [solution](4kyu/human_readable_duration_format)
- [4 By 4 Skyscrapers](https://www.codewars.com/kata/5671d975d81d6c1c87000022) - [solution (also Java)](4kyu/4_by_4_skyscrapers)
- [A Simplistic TCP Finite State Machine (FSM)](https://www.codewars.com/kata/54acc128329e634e9a000362) - [solution](4kyu/simplistic_tcp_fsm)
- [Counting Change Combinations](https://www.codewars.com/kata/541af676b589989aed0009e7) - [solution](4kyu/counting_change_combinations)
- [Sudoku Solution Validator](https://www.codewars.com/kata/529bf0e9bdf7657179000008) - [solution](4kyu/sudoku_solution_validator)
- [Sum Strings as Numbers](https://www.codewars.com/kata/5324945e2ece5e1f32000370) - [solution](4kyu/sum_strings_as_numbers)
- [Adding Big Numbers](https://www.codewars.com/kata/525f4206b73515bffb000b21) - [solution](4kyu/adding_big_numbers)

### C++

- [Permutations](https://www.codewars.com/kata/5254ca2719453dcc0b00027d) - [solution](4kyu/permutations)
- [Matrix Determinant](https://www.codewars.com/kata/52a382ee44408cea2500074c) - [solution](4kyu/matrix_determinant)
- [Infix to Postfix Converter](https://www.codewars.com/kata/52e864d1ffb6ac25db00017f) - [solution](4kyu/infix_to_postfix_converter)
- [Range Extraction](https://www.codewars.com/kata/51ba717bb08c1cd60f00002f) - [solution](4kyu/range_extraction)
- [Shortest Knight Path](https://www.codewars.com/kata/549ee8b47111a81214000941) - [solution](4kyu/shortest_knight_path)
- [Memoized Log Cutting](https://www.codewars.com/kata/54b058ce56f22dc6fe0011df) - [solution](4kyu/memoized_log_cutting)

### JS

- [Twice linear](https://www.codewars.com/kata/5672682212c8ecf83e000050) - [solution](4kyu/twice_linear)
- [Breadcrumb Generator](https://www.codewars.com/kata/563fbac924106b8bf7000046) - [solution](4kyu/breadcrumb_generator)
- [Priori Incantatem](https://www.codewars.com/kata/574d0b01b4b769b207000ca3) - [solution](4kyu/priori_incantatem)
- [ES5 Generators(i)](https://www.codewars.com/kata/53c29a6abb5187180d000b65) - [solution](4kyu/es5_generators_i)
- [Remove Zeros](https://www.codewars.com/kata/52aae14aa7fd03d57400058f) - [solution](4kyu/remove_zeros)
- [Sort binary tree by levels](https://www.codewars.com/kata/52bef5e3588c56132c0003bc) - [solution](4kyu/sort_binary_tree_by_levels)

### TS

- [Route Calculator](https://www.codewars.com/kata/581bc0629ad9ff9873000316) - [solution](4kyu/route_calculator)

### Java

- [Sum of Intervals](https://www.codewars.com/kata/52b7ed099cdc285c300001cd) - [solution](4kyu/sum_of_intervals)
- [Hamming Numbers](https://www.codewars.com/kata/526d84b98f428f14a60008da) - [solution](4kyu/hamming_numbers)

## 5 kyu

### Kotlin

- [Buddy Pairs](https://www.codewars.com/kata/59ccf051dcc4050f7800008f) - [solution](5kyu/buddy_pairs)
- [Simple assembler interpreter](https://www.codewars.com/kata/58e24788e24ddee28e000053) - [solution](5kyu/simple_assembler_interpreter)
- [Maximum subarray sum](https://www.codewars.com/kata/54521e9ec8e60bc4de000d6c) - [solution](5kyu/maximum_subarray_sum)
- [Product of consecutive Fib numbers](https://www.codewars.com/kata/5541f58a944b85ce6d00006a) - [solution](5kyu/product_of_consecutive_fib_numbers)

### C#

- [Moving Zeros To The End](https://www.codewars.com/kata/52597aa56021e91c93000cb0) - [solution](5kyu/moving_zeros_to_the_end)
- [Common Denominators](https://www.codewars.com/kata/54d7660d2daf68c619000d95) - [solution](5kyu/common_denominators)
- [Number of trailing zeros of N!](https://www.codewars.com/kata/52f787eb172a8b4ae1000a34) - [solution](5kyu/number_of_trailing_zeros_of_nf)
- [Human Readable Time](https://www.codewars.com/kata/52685f7382004e774f0001f7) - [solution](5kyu/human_readable_time)
- [RGB To Hex Conversion](https://www.codewars.com/kata/513e08acc600c94f01000001) - [solution](5kyu/rgb_to_hex_conversion)
- [Scramblies](https://www.codewars.com/kata/55c04b4cc56a697bb0000048) - [solution](5kyu/scramblies)

### Rust

- [Which x for that sum?](https://www.codewars.com/kata/5b1cd19fcd206af728000056) - [solution](5kyu/which_x_for_that_sum)
- [Perimeter of squares in a rectangle](https://www.codewars.com/kata/559a28007caad2ac4e000083) - [solution](5kyu/perimeter_of_squares_in_a_rectangle)
- [My smallest code interpreter (aka Brainf**k)](https://www.codewars.com/kata/526156943dfe7ce06200063e) - [solution](5kyu/my_smallest_code_interpreter_bf)
- [Count IP Addresses](https://www.codewars.com/kata/526989a41034285187000de4) - [solution](5kyu/count_ip_addresses)
- [Some Egyptian fractions](https://www.codewars.com/kata/54f8693ea58bce689100065f) - [solution](5kyu/some_egyptians_fractions)

### C

- [int32 to IPv4](https://www.codewars.com/kata/52e88b39ffb6ac53a400022e) - [solution](5kyu/i32_to_ipv4)
- [Convert A Hex String To RGB](https://www.codewars.com/kata/5282b48bb70058e4c4000fa7) - [solution](5kyu/convert_a_hex_string_to_rgb)
- [ISBN-10 Validation](https://www.codewars.com/kata/51fc12de24a9d8cb0e000001) - [solution](5kyu/isbn10_validation)

### C++

- [Is my friend cheating?](https://www.codewars.com/kata/5547cc7dcad755e480000004) - [solution](5kyu/is_my_friend_cheating)
- [Greed is Good](https://www.codewars.com/kata/5270d0d18625160ada0000e4) - [solution](5kyu/greed_is_good)
- [Snakes and Ladders](https://www.codewars.com/kata/587136ba2eefcb92a9000027) - [solution](5kyu/snakes_and_ladders)
- [The Clockwise Spiral](https://www.codewars.com/kata/536a155256eb459b8700077e) - [solution](5kyu/the_clockwise_spiral)
- [(Ready for) Prime Time](https://www.codewars.com/kata/521ef596c106a935c0000519) - [solution](5kyu/prime_time)

### Python

- [Weight for weight](https://www.codewars.com/kata/55c6126177c9441a570000cc) - [solution](5kyu/weight_for_weight)
- [Valid Parentheses](https://www.codewars.com/kata/52774a314c2333f0a7000688) - [solution](5kyu/valid_parentheses)
- [Simple Pig Latin](https://www.codewars.com/kata/520b9d2ad5c005041100000f) - [solution](5kyu/simple_pig_latin)
- [Sum of pairs](https://www.codewars.com/kata/54d81488b981293527000c8f) - [solution](5kyu/sum_of_pairs)
- [Memoized Fibonacci](https://www.codewars.com/kata/529adbf7533b761c560004e5) - [solution](5kyu/memoized_fibonacci)

### Haskell

- [Directions Reduction](https://www.codewars.com/kata/550f22f4d758534c1100025a) - [solution](5kyu/directions_reduction)
- [Pick peaks](https://www.codewars.com/kata/5279f6fe5ab7f447890006a7) - [solution](5kyu/pick_peaks)

### JS

- [Where my anagrams at?](https://www.codewars.com/kata/523a86aa4230ebb5420001e1) - [solution](5kyu/where_my_anagrams_at)
- [Rot13](https://www.codewars.com/kata/530e15517bc88ac656000716) - [solution](5kyu/rot13)
- [The Hashtag Generator](https://www.codewars.com/kata/52449b062fb80683ec000024) - [solution](5kyu/the_hashtag_generator)
- [Tic-Tac-Toe Checker](https://www.codewars.com/kata/525caa5c1bf619d28c000335) - [solution](5kyu/tic_tac_toe_checker)
- [Did I Finish my Sudoku?](https://www.codewars.com/kata/53db96041f1a7d32dc0004d2) - [solution](5kyu/did_i_finish_my_sudokou)
- [Calculating with Functions](https://www.codewars.com/kata/525f3eda17c7cd9f9e000b39) - [solution](5kyu/calculating_with_functions)
- [Vector class](https://www.codewars.com/kata/526dad7f8c0eb5c4640000a4) - [solution](5kyu/vector_class)
- [Halloween Sweets](https://www.codewars.com/kata/526bd612696e23c43a00032e) - [solution](5kyu/halloween_sweets)

### Swift

- [Esolang Interpreters #2 - Custom Smallfuck Interpreter](https://www.codewars.com/kata/58678d29dbca9a68d80000d7) - [solution](5kyu/esolang_interpreters_ii)

### Ruby

- [Best travel](https://www.codewars.com/kata/55e7280b40e1c4a06d0000aa) - [solution](5kyu/best_travel)
- [Primes in numbers](https://www.codewars.com/kata/54d512e62a5e54c96200019e) - [solution](5kyu/primes_in_numbers)

## 6 kyu

### Rust

- [Irreducible Sum of Rationals](https://www.codewars.com/kata/5517fcb0236c8826940003c9) - [solution](6kyu/irreducible_sum_of_rationals)
- [Help the bookseller !](https://www.codewars.com/kata/54dc6f5a224c26032800005c) - [solution](6kyu/help_the_bookseller)
- [Counting Duplicates](https://www.codewars.com/kata/54bf1c2cd5b56cc47f0007a1) - [solution](6kyu/counting_duplicates)
- [Decode the Morse code](https://www.codewars.com/kata/54b724efac3d5402db00065e) - [solution](6kyu/decode_the_morse_code)
- [Parabolic Arc Length](https://www.codewars.com/kata/562e274ceca15ca6e70000d3) - [solution](6kyu/parabolic_arc_length)
- [CamelCase Method](https://www.codewars.com/kata/587731fda577b3d1b0001196) - [solution](6kyu/camelcase_method)
- [Simple Substitution Cipher Helper](https://www.codewars.com/kata/52eb114b2d55f0e69800078d) - [solution](6kyu/simple_substitution_cipher_helper)

### C#

- [Find the odd int](https://www.codewars.com/kata/54da5a58ea159efa38000836) - [solution](6kyu/find_the_odd_int)
- [Rectangle into Squares](https://www.codewars.com/kata/55466989aeecab5aac00003e) - [solution](6kyu/rectangle_into_squares)
- [Does my number look big in this?](https://www.codewars.com/kata/5287e858c6b5a9678200083c) - [solution](6kyu/does_my_number_look_big_in_this)
- [Stop gninnipS My sdroW!](https://www.codewars.com/kata/5264d2b162488dc400000001) - [solution](6kyu/stop_gninnips_my_sdrow)
- [Count the smiley faces!](https://www.codewars.com/kata/583203e6eb35d7980400002a) - [solution](6kyu/count_the_smiley_faces)
- [Who likes it?](https://www.codewars.com/kata/5266876b8f4bf2da9b000362) - [solution](6kyu/who_likes_it)
- [Roman Numerals Encoder](https://www.codewars.com/kata/51b62bf6a9c58071c600001b) - [solution](6kyu/roman_numerals_encoder)
- [Roman Numerals Decoder](https://www.codewars.com/kata/51b6249c4612257ac0000005) - [solution](6kyu/roman_numerals_dekcoder)
- [Equal Sides Of An Array](https://www.codewars.com/kata/5679aa472b8f57fb8c000047) - [solution](6kyu/equal_sides_of_an_array)
- [The Supermarket Queue](https://www.codewars.com/kata/57b06f90e298a7b53d000a86) - [solution](6kyu/the_supermarket_queue)
- [LOTTO 6 aus 49 - 6 of 49](https://www.codewars.com/kata/57a98e8172292d977b000079) - [solution](6kyu/lotto_6_aus_49)
- [Length of missing array](https://www.codewars.com/kata/57b6f5aadb5b3d0ae3000611) - [solution](6kyu/length_of_missing_array)
- [Word a10n (abbreviation)](https://www.codewars.com/kata/5375f921003bf62192000746) - [solution](6kyu/word_a10n)

### Haskell

- [Take a Ten Minute Walk](https://www.codewars.com/kata/54da539698b8a2ad76000228) - [solution](6kyu/take_a_ten_minute_walk)
- [Multiples of 3 or 5](https://www.codewars.com/kata/514b92a657cdc65150000006) - [solution](6kyu/multiples_of_3_or_5)
- [Sum of Digits / Digital Root](https://www.codewars.com/kata/541c8630095125aba6000c00) - [solution](6kyu/sum_of_digits__digital_root)
- [Pyramid Array](https://www.codewars.com/kata/515f51d438015969f7000013) - [solution](6kyu/pyramid_array)
- [Make the Deadfish swim](https://www.codewars.com/kata/51e0007c1f9378fa810002a9) - [solution](6kyu/make_the_deadfish_swim)
- [Pascal's Triangle](https://www.codewars.com/kata/5226eb40316b56c8d500030f) - [solution](6kyu/pascals_triangle)

### Python

- [Bit Counting](https://www.codewars.com/kata/526571aae218b8ee490006f4) - [solution](6kyu/bit_counting)
- [Is a number prime?](https://www.codewars.com/kata/5262119038c0985a5b00029f) - [solution](6kyu/is_a_number_prime)
- [Bouncing Balls](https://www.codewars.com/kata/5544c7a5cb454edb3c000047) - [solution](6kyu/bouncing_balls)
- [Highest Scoring Word](https://www.codewars.com/kata/57eb8fcdf670e99d9b000272) - [solution](6kyu/highest_scoring_word)
- [Your order, please](https://www.codewars.com/kata/55c45be3b2079eccff00010f) - [solution](6kyu/your_order_please)
- [Find the unique number](https://www.codewars.com/kata/585d7d5adb20cf33cb000235) - [solution](6kyu/find_the_unique_number)
- [Split Strings](https://www.codewars.com/kata/515de9ae9dcfc28eb6000001) - [solution](6kyu/split_strings)
- [Find the missing term in an Arithmetic Progression](https://www.codewars.com/kata/52de553ebb55d1fca3000371) - [solution](6kyu/find_the_missing_term_in_arith_prog)
- [Fat Fingers](https://www.codewars.com/kata/5aa99584fd5777ee9a0001f1) - [solution](6kyu/fat_fingers)

### Kotlin

- [Playing with digits](https://www.codewars.com/kata/5552101f47fc5178b1000050) - [solution](6kyu/playing_with_digits)
- [Ball Upwards](https://www.codewars.com/kata/566be96bb3174e155300001b) - [solution](6kyu/ball_upwards)

### C++

- [Statistics for an Athletic Association](https://www.codewars.com/kata/55b3425df71c1201a800009c) - [solution](6kyu/statistics_for_an_athletic_association)
- [Linked Lists - Length & Count](https://www.codewars.com/kata/55beec7dd347078289000021) - [solution](6kyu/linked_lists_length_count)
- [Design a Simple Automaton (Finite State Machine)](https://www.codewars.com/kata/5268acac0d3f019add000203) - [solution](6kyu/design_a_simple_automaton)

### JS

- [Are they the "same"?](https://www.codewars.com/kata/550498447451fbbd7600041c/) - [solution](6kyu/are_they_the_same)
- [Create Phone Number](https://www.codewars.com/kata/525f50e3b73515a6db000b83) - [solution](6kyu/create_phone_number)
- [Build Tower](https://www.codewars.com/kata/576757b1df89ecf5bd00073b) - [solution](6kyu/build_tower)
- [Convert string to camel case](https://www.codewars.com/kata/517abf86da9663f1d2000003) - [solution](6kyu/convert_string_to_camel_case)
- [Delete occurrences of an element if it occurs more than n times](https://www.codewars.com/kata/554ca54ffa7d91b236000023) - [solution](6kyu/delete_occurrences_if_occurs)
- [Reverse polish notation calculator](https://www.codewars.com/kata/52f78966747862fc9a0009ae) - [solution](6kyu/rpn_calculator)
- [Halloween: trick or treat!](https://www.codewars.com/kata/581302bbbee85709d00002ae) - [solution](6kyu/halloween_trick_or_treat)
- [Build Tower Advanced](https://www.codewars.com/kata/57675f3dedc6f728ee000256) - [solution](6kyu/build_tower_advanced)
- [Evil Autocorrect Prank](https://www.codewars.com/kata/538ae2eb7a4ba8c99b000439) - [solution](6kyu/evil_autocorrect_prank)
- [The 5 Love Languages](https://www.codewars.com/kata/5aa7a581fd8c06b552000177) - [solution](6kyu/the_5_love_languages)

### Java

- [Consecutive strings](https://www.codewars.com/kata/56a5d994ac971f1ac500003e) - [solution](6kyu/consecutive_strings)
- [Duplicate Encoder](https://www.codewars.com/kata/54b42f9314d9229fd6000d9c) - [solution](6kyu/duplicate_encoder)
- [Find the missing letter](https://www.codewars.com/kata/5839edaa6754d6fec10000a2) - [solution](6kyu/find_the_missing_letter)
- [Two Sum](https://www.codewars.com/kata/52c31f8e6605bcc646000082) - [solution](6kyu/two_sum)

### C

- [IP Validation](https://www.codewars.com/kata/515decfd9dcfc23bb6000006) - [solution](6kyu/ip_validation)
- [Cheat or you shall not pass: easy](https://www.codewars.com/kata/58f5ae82164573de7c00038c) - [solution](6kyu/cheat_or_you_shall_not_pass_easy)

### Go

- [Multiplication table](https://www.codewars.com/kata/534d2f5b5371ecf8d2000a08) - [solution](6kyu/multiplication_table)

### Swift

- [Give me a Diamond](https://www.codewars.com/kata/5503013e34137eeeaa001648) - [solution](6kyu/give_me_a_diamond)
- [Esolang Interpreters #1 - Introduction to Esolangs and My First Interpreter (MiniStringFuck)](https://www.codewars.com/kata/586dd26a69b6fd46dd0000c0) - [solution](6kyu/esolang_interpreters_i)

### TS

- [Valid Braces](https://www.codewars.com/kata/5277c8a221e209d3f6000b56) - [solution](6kyu/valid_braces)

### Pascal

- [Detect Pangram](https://www.codewars.com/kata/545cedaa9943f7fe7b000048) - [solution](6kyu/detect_pangram)

### Ruby

- [Break camelCase](https://www.codewars.com/kata/5208f99aee097e6552000148) - [solution](6kyu/break_camel_case)

## 7 kyu

### JS

- [Isograms](https://www.codewars.com/kata/54ba84be607a92aa900000f1) - [solution](7kyu/isograms)
- [Highest and Lowest](https://www.codewars.com/kata/554b4ac871d6813a03000035) - [solution](7kyu/highest_and_lowest)
- [Growth of a Population](https://www.codewars.com/kata/563b662a59afc2b5120000c6) - [solution](7kyu/growth_of_a_population)
- [Pirate Code](https://www.codewars.com/kata/59e77930233243a7b7000026) - [solution](7kyu/pirate_code)
- [Find the divisors!](https://www.codewars.com/kata/544aed4c4a30184e960010f4) - [solution](7kyu/find_the_divisors)
- [Categorize New Member](https://www.codewars.com/kata/5502c9e7b3216ec63c0001aa) - [solution](7kyu/categorize_new_member)
- [Distributing Candies Fairly](https://www.codewars.com/kata/59901cd68fc658ab6c000025) - [solution](7kyu/distributing_candies_fairly)

### C++

- [Is this a triangle?](https://www.codewars.com/kata/56606694ec01347ce800001b) - [solution](7kyu/is_this_a_triangle)
- [Sum of angles](https://www.codewars.com/kata/5a03b3f6a1c9040084001765) - [solution](7kyu/sum_of_angles)
- [Are the numbers in order?](https://www.codewars.com/kata/56b7f2f3f18876033f000307) - [solution](7kyu/are_the_numbers_in_order)

### PHP

- [Printer Errors](https://www.codewars.com/kata/56541980fa08ab47a0000040) - [solution](7kyu/printer_errors)

### Python

- [Bumps in the Road](https://www.codewars.com/kata/57ed30dde7728215300005fa) - [solution](7kyu/bumps_in_the_road)
- [Ones and Zeros](https://www.codewars.com/kata/578553c3a1b8d5c40300037c) - [solution](7kyu/ones_and_zeros)
- [Odd or Even?](https://www.codewars.com/kata/5949481f86420f59480000e7) - [solution](7kyu/odd_or_even)
- [Palindrome chain length](https://www.codewars.com/kata/525f039017c7cd0e1a000a26) - [solution](7kyu/palindrome_chain_length)
- [Inspiring Strings](https://www.codewars.com/kata/5939ab6eed348a945f0007b2) - [solution](7kyu/inspiring_strings)
- [Anagram Detection](https://www.codewars.com/kata/529eef7a9194e0cbc1000255) - [solution](7kyu/anagram_detection)
- [Sorted? yes? no? how?](https://www.codewars.com/kata/580a4734d6df748060000045) - [solution](7kyu/sorted_yes_no_how)
- [Hit Count](https://www.codewars.com/kata/57b6f850a6fdc76523001162) - [solution](7kyu/hit_count)
- [Don't give me five!](https://www.codewars.com/kata/5813d19765d81c592200001a) - [solution](7kyu/dont_give_me_five)

### C#

- [Beginner Series #3 Sum of Numbers](https://www.codewars.com/kata/55f2b110f61eb01779000053) - [solution](7kyu/beginner_series_iii)
- [Find the next perfect square!](https://www.codewars.com/kata/56269eb78ad2e4ced1000013) - [solution](7kyu/find_the_next_perfect_square)
- [Vowel Count](https://www.codewars.com/kata/54ff3102c1bad923760001f3) - [solution](7kyu/vowel_count)
- [Credit Card Mask](https://www.codewars.com/kata/5412509bd436bd33920011bc) - [solution](7kyu/credit_card_mask)
- [Shortest Word](https://www.codewars.com/kata/57cebe1dc6fdc20c57000ac9) - [solution](7kyu/shortest_word)
- [Jaden Casing Strings](https://www.codewars.com/kata/5390bac347d09b7da40006f6) - [solution](7kyu/jaden_casing_strings)
- [Deodorant Evaporator](https://www.codewars.com/kata/5506b230a11c0aeab3000c1f) - [solution](7kyu/deodorant_evaporator)
- [Parts of a list](https://www.codewars.com/kata/56f3a1e899b386da78000732) - [solution](7kyu/parts_of_a_list)
- [Mumbling](https://www.codewars.com/kata/5667e8f4e3f572a8f2000039) - [solution](7kyu/mumbling)
- [Binary Addition](https://www.codewars.com/kata/551f37452ff852b7bd000139) - [solution](7kyu/binary_addition)
- [Sum of two lowest positive integers](https://www.codewars.com/kata/558fc85d8fd1938afb000014) - [solution](7kyu/sum_of_two_lowest_positive_integers)
- [Digital cypher vol 2](https://www.codewars.com/kata/592edfda5be407b9640000b2) - [solution](7kyu/digital_cypher_vol_2)
- [List Filtering](https://www.codewars.com/kata/53dbd5315a3c69eed20002dd) - [solution](7kyu/list_filtering)
- [Remove the minimum](https://www.codewars.com/kata/563cf89eb4747c5fb100001b) - [solution](7kyu/remove_the_minimum)

### Haskell

- [Fibonacci](https://www.codewars.com/kata/57a1d5ef7cb1f3db590002af) - [solution](7kyu/fibonacci)
- [Product of Array Items](https://www.codewars.com/kata/5901f361927288d961000013) - [solution](7kyu/product_of_array_items)
- [Make a function that does arithmetic!](https://www.codewars.com/kata/583f158ea20cfcbeb400000a) - [solution](7kyu/make_a_function_that_does_arithmetic)
- [Exes and Ohs](https://www.codewars.com/kata/55908aad6620c066bc00002a) - [solution](7kyu/exes_and_ohs)

### Kotlin

- [Sum of odd numbers](https://www.codewars.com/kata/55fd2d567d94ac3bc9000064) - [solution](7kyu/sum_of_odd_numbers)
- [Number of People in the Bus](https://www.codewars.com/kata/5648b12ce68d9daa6b000099) - [solution](7kyu/number_of_people_in_the_bus)
- [Complementary DNA](https://www.codewars.com/kata/554e4a2f232cdd87d9000038) - [solution](7kyu/complementary_dna)
- [Small enough? - Beginner](https://www.codewars.com/kata/57cc981a58da9e302a000214) - [solution](7kyu/small_enough_beginner)
- [Rotate for a Max](https://www.codewars.com/kata/56a4872cbb65f3a610000026) - [solution](7kyu/rotate_for_a_max)
- [Predict your age!](https://www.codewars.com/kata/5aff237c578a14752d0035ae) - [solution](7kyu/predict_your_age)

### C

- [Looking for a benefactor](https://www.codewars.com/kata/569b5cec755dd3534d00000f) - [solution](7kyu/looking_for_a_benefactor)
- [Speed Control](https://www.codewars.com/kata/56484848ba95170a8000004d) - [solution](7kyu/speed_control)
- [Most digits](https://www.codewars.com/kata/58daa7617332e59593000006) - [solution](7kyu/most_digits)

### Rust

- [Maximum Length Difference](https://www.codewars.com/kata/5663f5305102699bad000056) - [solution](7kyu/maximum_length_difference)
- [Digital cypher](https://www.codewars.com/kata/592e830e043b99888600002d) - [solution](7kyu/digital_cypher)

### Java

- [Regex validate PIN code](https://www.codewars.com/kata/55f8a9c06c018a0d6e000132) - [solution](7kyu/regex_validate_pin_code)

### Swift

- [Fizz Buzz Cuckoo Clock](https://www.codewars.com/kata/58485a43d750d23bad0000e6) - [solution](7kyu/fizz_buzz_cuckoo_clock)
- [Resistor Color Codes](https://www.codewars.com/kata/57cf3dad05c186ba22000348) - [solution](7kyu/resistor_color_codes)

## 8 kyu

### JS

- [Square(n) Sum](https://www.codewars.com/kata/515e271a311df0350d00000f) - [solution](8kyu/square_n_sum)
- [Student's Final Grade](https://www.codewars.com/kata/5ad0d8356165e63c140014d4) - [solution](8kyu/students_final_grade)
- [Grasshopper - Personalized Message](https://www.codewars.com/kata/5772da22b89313a4d50012f7) - [solution](8kyu/grasshopper_personalized_message)
- [altERnaTIng cAsE <=> ALTerNAtiNG CaSe](https://www.codewars.com/kata/56efc695740d30f963000557) - [solution](8kyu/alternating_case)

### C

- [String repeat](https://www.codewars.com/kata/57a0e5c372292dd76d000d7e) - [solution](8kyu/string_repeat)
- [Parse nice int from char problem](https://www.codewars.com/kata/557cd6882bfa3c8a9f0000c1) - [solution](8kyu/parse_nice_int_from_char)
- [Get Nth Even Number](https://www.codewars.com/kata/5933a1f8552bc2750a0000ed) - [solution](8kyu/get_nth_even_number)
- [Get the mean of an array](https://www.codewars.com/kata/563e320cee5dddcf77000158) - [solution](8kyu/get_the_mean_of_an_array)
- [Fake Binary](https://www.codewars.com/kata/57eae65a4321032ce000002d) - [solution](8kyu/fake_binary)
- [Is the string uppercase?](https://www.codewars.com/kata/56cd44e1aa4ac7879200010b) - [solution](8kyu/is_the_string_uppercase)

### C++

- [Counting sheep...](https://www.codewars.com/kata/54edbc7200b811e956000556) - [solution](8kyu/counting_sheep)
- [Reversed Strings](https://www.codewars.com/kata/5168bb5dfe9a00b126000018) - [solution](8kyu/reversed_strings)
- [Multiply](https://www.codewars.com/kata/50654ddff44f800200000004) - [solution](8kyu/multiply)
- [Will you make it?](https://www.codewars.com/kata/5861d28f124b35723e00005e) - [solution](8kyu/will_you_make_it)
- [Is it a palindrome?](https://www.codewars.com/kata/57a1fd2ce298a731b20006a4) - [solution](8kyu/is_it_a_palindrome)
- [Sum without highest and lowest number](https://www.codewars.com/kata/576b93db1129fcf2200001e6) - [solution](8kyu/sum_without_highest_and_lowest)

### Haskell

- [Even or Odd](https://www.codewars.com/kata/53da3dbb4a5168369a0000fe) - [solution](8kyu/even_or_odd)
- [Convert boolean values to strings 'Yes' or 'No'.](https://www.codewars.com/kata/53369039d7ab3ac506000467) - [solution](8kyu/convert_boolean_values_to_strings)
- [Century From Year](https://www.codewars.com/kata/5a3fe3dde1ce0e8ed6000097) - [solution](8kyu/century_from_year)
- [Return Negative](https://www.codewars.com/kata/55685cd7ad70877c23000102) - [solution](8kyu/return_negative)
- [Opposite number](https://www.codewars.com/kata/56dec885c54a926dcd001095) - [solution](8kyu/opposite_number)
- [Keep Hydrated!](https://www.codewars.com/kata/582cb0224e56e068d800003c) - [solution](8kyu/keep_hydrated)
- [Calculate average](https://www.codewars.com/kata/57a2013acf1fa5bfc4000921) - [solution](8kyu/calculate_average)
- [How many lightsabers do you own?](https://www.codewars.com/kata/51f9d93b4095e0a7200001b8) - [solution](8kyu/how_many_lightsabers)
- [Exclusive "or" (xor) Logical Operator](https://www.codewars.com/kata/56fa3c5ce4d45d2a52001b3c) - [solution](8kyu/xor)

### Python

- [Correct the mistakes of the character recognition software](https://www.codewars.com/kata/577bd026df78c19bca0002c0) - [solution](8kyu/correct_mistakes_of_character_recognition)
- [Sum of positive](https://www.codewars.com/kata/5715eaedb436cf5606000381) - [solution](8kyu/sum_of_positive)
- [MakeUpperCase](https://www.codewars.com/kata/57a0556c7cb1f31ab3000ad7) - [solution](8kyu/make_upper_case)
- [Sum Arrays](https://www.codewars.com/kata/53dc54212259ed3d4f00071c) - [solution](8kyu/sum_arrays)
- [5 without numbers !!](https://www.codewars.com/kata/59441520102eaa25260000bf) - [solution](8kyu/five_without_numbers)

### Pascal

- [Beginner Series #4 Cockroach](https://www.codewars.com/kata/55fab1ffda3e2e44f00000c6) - [solution](8kyu/beginner_series_iv)
- [Grasshopper - Summation](https://www.codewars.com/kata/55d24f55d7dd296eb9000030) - [solution](8kyu/grasshopper_summation)

### Kotlin

- [Beginner Series #2 Clock](https://www.codewars.com/kata/55f9bca8ecaa9eac7100004a) - [solution](8kyu/beginner_series_ii)
- [Opposites Attract](https://www.codewars.com/kata/555086d53eac039a2a000083) - [solution](8kyu/opposites_attract)
- [Beginner - Lost Without a Map](https://www.codewars.com/kata/57f781872e3d8ca2a000007e) - [solution](8kyu/beginner_lost_without_a_map)

### Rust

- [Difference of Volumes of Cuboids](https://www.codewars.com/kata/58cb43f4256836ed95000f97) - [solution](8kyu/difference_of_volumes_of_cuboids)
- [Find the smallest integer in the array](https://www.codewars.com/kata/55a2d7ebe362935a210000b2) - [solution](8kyu/find_the_smallest_integer_in_the_array)

### PHP

- [Remove First and Last Character](https://www.codewars.com/kata/56bc28ad5bdaeb48760009b0) - [solution](8kyu/remove_first_and_last_character)

### C#

- [Convert number to reversed array of digits](https://www.codewars.com/kata/5583090cbe83f4fd8c000051) - [solution](8kyu/convert_number_to_reversed_digits)
- [Grasshopper - Grade book](https://www.codewars.com/kata/55cbd4ba903825f7970000f5) - [solution](8kyu/grasshopper_grade_book)
- [A Needle in the Haystack](https://www.codewars.com/kata/56676e8fabd2d1ff3000000c) - [solution](8kyu/a_needle_in_the_haystack)
- [Enumerable Magic #3 - Does My List Include This?](https://www.codewars.com/kata/545991b4cbae2a5fda000158) - [solution](8kyu/enumerable_magic_iii)
- [Exclamation marks series #4: Remove all exclamation marks from sentence but ensure a exclamation mark at the end of string](https://www.codewars.com/kata/57faf12b21c84b5ba30001b0) - [solution](8kyu/exclamation_marks)
- [To square(root) or not to square(root)](https://www.codewars.com/kata/57f6ad55cca6e045d2000627) - [solution](8kyu/to_square_or_not_to_square)

### Java

- [Jenny's secret message](https://www.codewars.com/kata/55225023e1be1ec8bc000390) - [solution](8kyu/jennys_secret_message)
- [Count the Monkeys!](https://www.codewars.com/kata/56f69d9f9400f508fb000ba7) - [solution](8kyu/count_the_monkeys)

---

- [](https://www.codewars.com/kata/) - [solution]()
