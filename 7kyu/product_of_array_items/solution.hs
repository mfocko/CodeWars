module Product where

product' :: [Integer] -> Maybe Integer
product' [] = Nothing
product' xs = Just $ foldl1 (*) xs
