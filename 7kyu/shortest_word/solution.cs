public class Kata
{
  public static int FindShort(string s)
  {
    int shortest = s.Length;

    string[] words = s.Split(' ');
    foreach (string word in words)
      if (word.Length < shortest) shortest = word.Length;

    return shortest;
  }
}
