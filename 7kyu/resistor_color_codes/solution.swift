let COLOR_MAPPING: [String: Int] = [
  "black": 0,
  "brown": 1,
  "red": 2,
  "orange": 3,
  "yellow": 4,
  "green": 5,
  "blue": 6,
  "violet": 7,
  "gray": 8,
  "white": 9
]

func formatOhms(_ ohms: Int) -> String {
  func formatBig(n: Int, base: Int, suffix: String) -> String {
    var short: String
    if (n % base == 0) {
      short = String(n / base)
    } else {
      short = String(Double(ohms) / Double(base))
    }

    return "\(short)\(suffix)"
  }

  if ohms < 1000 {
    return String(ohms)
  } else if ohms < 1000000 {
    return formatBig(n: ohms, base: 1000, suffix: "k")
  }
  return formatBig(n: ohms, base: 1000000, suffix: "M")
}

func decodeResistorColors(_ bands: String) -> String {
  var (ohms, tolerance) = (0, 20)
  let splitBands = bands.split(separator: " ")

  for i in 0..<2 {
    ohms *= 10
    ohms += COLOR_MAPPING[String(splitBands[i])]!
  }

  for _ in 0..<COLOR_MAPPING[String(splitBands[2])]! {
    ohms *= 10
  }

  if splitBands.count > 3 {
    switch String(splitBands[3]) {
      case "gold":
        tolerance = 5
      case "silver":
        tolerance = 10
      default:
        tolerance = 20
    }
  }

  return "\(formatOhms(ohms)) ohms, \(tolerance)%"
}
