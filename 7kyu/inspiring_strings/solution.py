def longest_word(string_of_words):
    words = string_of_words.split(' ')

    result = (words[0], len(words[0]))

    for word in words:
        if len(word) >= result[1]:
            result = (word, len(word))

    return result[0]
