fun people(busStops: Array<Pair<Int, Int>>) : Int =
    busStops.fold(0) { inBus, (gotOn, gotOff) -> inBus + gotOn - gotOff }
