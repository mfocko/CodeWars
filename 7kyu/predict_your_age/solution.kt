fun predictAge(vararg ages: Int): Int = Math.floor(
  Math.sqrt(
      ages.sumOf { it * it }.toDouble()
  ).div(2)
).toInt()
