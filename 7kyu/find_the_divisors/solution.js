function divisors(integer) {
  let sqrt = Math.floor(Math.sqrt(integer));
  let left = [], right = [];
  for (let i = 2; i <= sqrt; i++) {
    if (integer % i == 0) {
      left.push(i);
      if (i !== integer / i) {
        right.splice(0, 0, integer / i);
      }
    }
  }
  if (left.length != 0) {
    return left.concat(right);
  } else {
    return `${integer} is prime`;
  }
}
