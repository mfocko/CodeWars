using System;

public static class Kata
{
  public static string AddBinary(int a, int b)
  {
    var sum = a + b;
    var result = "";

    while (sum > 0) {
      result = $"{sum % 2}" + result;
      sum /= 2;
    }

    return result;
  }
}
