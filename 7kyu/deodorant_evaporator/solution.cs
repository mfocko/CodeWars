public class Evaporator {
  public static int evaporator(double content, double evap_per_day, double threshold) {
    double c = 1, t = threshold / 100, e  = 1 - evap_per_day / 100;
    int i = 0;
    while (c >= t) {
      c *= e;
      i++;
    }
    return i;
  }
}
