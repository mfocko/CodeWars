function printerError($s) {
    $length = strlen($s);
    $errors = 0;

    for ($i = 0; $i < $length; $i++) {
      if ($s[$i] > 'm') $errors++;
    }
    return "$errors/$length";
}
