function nbYear(p0, percent, aug, p) {
    percent = percent / 100;

    let count = 0;
    while (p0 < p) {
      p0 *= 1 + percent;
      p0 += aug;
      count++;
    }
    return count;
}
