package dna

fun makeComplement(dna : String) : String = dna.map { it ->
    when (it) {
        'A' -> 'T'
        'T' -> 'A'
        'C' -> 'G'
        'G' -> 'C'
        else -> it
    }
}.joinToString("")
