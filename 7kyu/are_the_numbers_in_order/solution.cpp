bool isAscOrder(std::vector<int> arr)
{
    for (int i = 0, length = arr.size() - 1; i < length; i++)
        if (arr[i] > arr[i + 1])
            return false;
    return true;
}
