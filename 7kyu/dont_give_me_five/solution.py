def dont_give_me_five(start,end):
    return len(list(filter(lambda s: not '5' in s, map(str, range(start, end + 1)))))
