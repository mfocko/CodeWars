module Codewars.Kata.XO where

import Data.Char

-- | Returns true if the number of
-- Xs is equal to the number of Os
-- (case-insensitive)
xo :: String -> Bool
xo str = xs == os
  where counter (x, o) 'x' = (x + 1, o)
        counter (x, o) 'o' = (x, o + 1)
        counter counts  _  = counts
        (xs, os) = foldl counter (0, 0) $ map toLower str
