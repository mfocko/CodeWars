using System.Collections.Generic;
using System.Linq;

public class Kata {
  public static IEnumerable<int> KeyToCiphers(int key) =>
    key.ToString().Select(
      c => c - '0'
    );

  public static string Decode(int[] code, int key) {
    var ciphers = KeyToCiphers(key);
    var result = "";

    for (var i = 0; i < code.Length; i++) {
      result += (char) ('a' - 1 + code[i] - ciphers.ElementAt(i % ciphers.Count()));
    }

    return result;
  }
}
