function amaroPlan(pirateNum){
  let total = 20 * pirateNum;

  let result = [];
  result.push(total - Math.floor((pirateNum - 1) / 2));

  for (let i = 1; i < pirateNum; i++) {
    result.push((i + 1) % 2);
  }

  return result;
}
