func parseTime(time: String) -> (Int, Int) {
  let parts = time.split(separator: ":")
  return (Int(parts[0])!, Int(parts[1])!)
}

func fizzBuzzCuckooClock(_ time: String) -> String {
  let (hour, minute) = parseTime(time: time)

  if minute % 15 == 0 {
    if minute == 0 {
      let count = hour % 12
      var result = String(repeating: "Cuckoo ", count: (count != 0) ? count : 12)
      result.removeLast(1)
      return result
    } else if minute == 30 {
      return "Cuckoo"
    }

    return "Fizz Buzz"
  } else if minute % 5 == 0 {
    return "Buzz"
  } else if minute % 3 == 0 {
    return "Fizz"
  }

  return "tick"
}
