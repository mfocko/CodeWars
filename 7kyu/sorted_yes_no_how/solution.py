def is_sorted_and_how(arr):
    last = "ascending" if arr[0] <= arr[1] else "descending"
    for i in range(1, len(arr) - 1):
        if arr[i] <= arr[i + 1] and last != "ascending":
            return "no"
        elif arr[i] >= arr[i + 1] and last != "descending":
            return "no"
    return "yes, " + last
