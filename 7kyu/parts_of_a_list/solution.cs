public class PartList
{
    public static string[][] Partlist(string[] arr)
    {
        string[][] result = new string[arr.Length - 1][];

        for (int i = 0, upTo = arr.Length - 1; i < upTo; i++)
        {
          result[i] = new string[2];
          result[i][0] = "";
          for (int j = 0; j <= i; j++) result[i][0] += arr[j] + " ";
          result[i][0] = result[i][0].Trim();
          result[i][1] = "";
          for (int j = i + 1; j < arr.Length; j++) result[i][1] += arr[j] + " ";
          result[i][1] = result[i][1].Trim();
        }
        return result;
    }
}
