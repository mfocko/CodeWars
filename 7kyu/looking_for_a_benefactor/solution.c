#include <stdio.h>
#include <math.h>

long long newAvg(double *arr, size_t szArray, double navg)
{
   double sum = 0.0;

   for (size_t i = 0; i < szArray; sum += arr[i++]);

   long long result = (long long) ceil(navg * (szArray + 1) - sum);

   if (result <= 0) return -1;
   else return result;
}
