module Kata where

data Operation = Add | Divide | Multiply | Subtract deriving (Eq, Show, Enum, Bounded)

arithmetic :: Fractional a => a -> a -> Operation -> a
arithmetic a b operator = case operator of
                            Add -> (a + b)
                            Divide -> (a / b)
                            Multiply -> (a * b)
                            Subtract -> (a - b)
