using System;
public class Sum
{
    public int GetSum(int a, int b)
    {
    if (a > b) {
        var temp = a;
        a = b;
        b = temp;
    }

    return (a + b) * (Math.Abs(b - a) + 1) / 2;
    }
}
