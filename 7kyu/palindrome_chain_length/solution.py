def palindrome_chain_length(n):
    c = 0
    r = int(str(n)[::-1])

    while n != r:
        n += r
        r = int(str(n)[::-1])
        c += 1

    return c
