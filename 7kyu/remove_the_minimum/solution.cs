using System;
using System.Collections.Generic;
using System.Linq;

public class Remover
{
  public static List<int> RemoveSmallest(List<int> numbers)
  {
    if (numbers.Count == 0) return numbers;

    List<int> new_numbers = new List<int>(numbers);

    int lowest = new_numbers[0];

    foreach (int e in numbers)
    {
      if (e < lowest) lowest = e;
    }

    new_numbers.Remove(lowest);

    return new_numbers;
  }
}
