use std::cmp;

struct Extremes {
    min: i128,
    max: i128
}

fn mx_dif_lg(a1: Vec<&str>, a2: Vec<&str>) -> i32 {
    if a1.len() == 0 || a2.len() == 0 {
        return -1
    }

    fn mx(strings: &Vec<&str>) -> Extremes {
        Extremes {
            min: strings.iter().map(|&x| x.len()).min().unwrap() as i128,
            max: strings.iter().map(|&x| x.len()).max().unwrap() as i128,
        }
    }

    let a1_extremes = mx(&a1);
    let a2_extremes = mx(&a2);

    cmp::max(
        (a1_extremes.min - a2_extremes.max).abs() as i32,
        (a1_extremes.max - a2_extremes.min).abs() as i32
    )
}
