module Fibonacci where

fib :: Int -> Int
fib n = snd $ foldl (\(prev, curr) _ -> (curr, prev + curr)) (1, 1) [3..n]
