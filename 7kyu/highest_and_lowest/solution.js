function highAndLow(numbers){
  numbers = numbers.split(' ');
  let min = parseInt(numbers[0]),
      max = parseInt(numbers[0]);
  for (let i = 1; i < numbers.length; i++) {
    let n = parseInt(numbers[i]);
    if (n < min) min = n;
    else if (n > max) max = n;
  }
  return `${max} ${min}`;
}
