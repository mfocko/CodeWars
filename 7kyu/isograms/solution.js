function isIsogram(str){
    str = str.toLowerCase();
    let encountered = new Set();

    for (let character of str.split('')) {
      if (!encountered.has(character)) {
        encountered.add(character);
      } else {
        return false;
      }
    }

    return true;
}
