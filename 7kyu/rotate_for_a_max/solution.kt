package maxrot

fun maxRot(n: Long): Long {
    // keep length of n
    val length = n.toString().length
    var firstDigit = Math.pow(10.toDouble(), (length - 1).toDouble()).toLong()

    var number = n // keep modified n
    var maxN = n   // keep maximum

    // rotate left for the length of n
    for (x in 0 until length) {
        val highest = number.div(firstDigit).rem(10)
        val remainder = number.div(firstDigit).div(10).times(firstDigit).times(10)

        number = remainder.plus(number.rem(firstDigit).times(10).plus(highest))
        firstDigit = firstDigit.div(10)

        if (number > maxN) {
            maxN = number
        }
    }

    // return maximum
    return maxN
}
