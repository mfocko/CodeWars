function distribute(m, n) {
  if (n <= 0) {
    return [];
  }

  let result = Array(n);
  if (m <= 0) {
    result.fill(0);
  } else {

    result.fill(Math.floor(m / n));
    m %= n;

    for (let i = 0; i < m; i++) {
      result[i]++;
    }
  }

  return result;
}
