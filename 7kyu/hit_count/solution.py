def counter_effect(hit_count):
    result = []
    for num in map(int, hit_count):
        result.append(list(range(num + 1)))
    return result
