using System;

public class Kata
{
  public static long FindNextSquare(long num)
  {
    double root = Math.Sqrt(num);
    if (root - (int) root > double.Epsilon)
    {
      return -1;
    }

    return (long) Math.Pow(Math.Round(root) + 1, 2);
  }
}
