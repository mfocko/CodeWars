using System;
using System.Linq;

public static class JadenCase
{
  public static string ToJadenCase(this string phrase)
  {
    return String.Join(" ", phrase.Split(' ').Select(word => word.First().ToString().ToUpper() + word.Substring(1)));
  }
}
