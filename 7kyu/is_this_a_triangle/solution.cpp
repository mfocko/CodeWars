namespace Triangle
{
    bool isTriangle(int ia, int ib, int ic)
    {
        long long a = ia;
        long long b = ib;
        long long c = ic;

        if (a == 0 || b == 0 || c == 0) return false;
        else if (a == b && b == c) return true;
        else if (a + b <= c) return false;
        else if (a + c <= b) return false;
        else if (b + c <= a) return false;

        return true;
    }
}
