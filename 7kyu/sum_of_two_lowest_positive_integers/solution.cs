public static class Kata
{
  public static int sumTwoSmallestNumbers(int[] numbers)
  {
    var smallest = new int[] {numbers[0], numbers[1]};

    if (smallest[0] > smallest[1]) {
      var temp = smallest[0];
      smallest[0] = smallest[1];
      smallest[1] = temp;
    }

    for (var i = 2; i < numbers.GetLength(0); i++) {
      var number = numbers[i];
      if (number < 0) continue;
      else if (number < smallest[0]) {
        smallest[1] = smallest[0];
        smallest[0] = number;
      } else if (number < smallest[1]) {
        smallest[1] = number;
      }
    }

    return smallest[0] + smallest[1];
  }
}
