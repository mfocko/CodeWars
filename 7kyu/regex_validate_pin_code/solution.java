public class Solution {
  public static boolean validatePin(String pin) {
    int length = pin.length();

    if (length != 4 && length != 6) {
      return false;
    } else {
      for (int i = 0; i < length; i++) {
        if (!Character.isDigit(pin.charAt(i))) {
          return false;
        }
      }
    }
    return true;
  }
}
