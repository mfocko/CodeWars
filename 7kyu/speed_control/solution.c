#include <math.h>

int gps(int s, double* x, int sz) {
  if (sz < 2) {
    return 0;
  }

  double maxDiff = x[1] - x[0];
  int maxIndex = 0;

  for (int i = 0; i < sz - 1; i++) {
    double diff = x[i + 1] - x[i];
    if (diff > maxDiff) {
      maxDiff = diff;
      maxIndex = i;
    }
  }

  return floor((3600 * maxDiff) / s);
}
