using System;

public class Accumul
{
    public static String Accum(string s)
    {
        var result = "";
        var count = 1;

        foreach (var c in s)
        {
            result += Char.ToUpper(c);
            for (var i = 1; i < count; i++) result += Char.ToLower(c);
            result += "-";
            count++;
        }
        return result.Trim('-');
    }
}
