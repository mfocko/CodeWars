#include <stddef.h>
#include <math.h>

int find_longest(int *numbers, size_t numbers_size)
{
    double digits = floor(log10(fabs(numbers[0])));
    int number = numbers[0];

    for (size_t i = 1; i < numbers_size; i++)
    {
      double digits_now = floor(log10(fabs(numbers[i])));
      if (digits_now > digits)
      {
        digits = digits_now;
        number = numbers[i];
      }
    }

    return number;
}
