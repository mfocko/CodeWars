fn get_ciphers(mut n: i32) -> Vec<i32> {
    let mut result = Vec::new();

    while n > 0 {
        result.insert(0, n % 10);
        n = n / 10;
    }

    return result;
}

fn encode(msg: String, n: i32) -> Vec<i32> {
    let ciphers = get_ciphers(n);
    let length = ciphers.len();

    msg.as_bytes().iter().enumerate().map(|(i, x)| {
        *x as i32 + ciphers[i % length] - 'a' as i32 + 1
    }).collect()
}
